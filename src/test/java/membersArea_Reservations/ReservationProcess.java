package membersArea_Reservations;

import java.util.List;
import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.Cookie;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.testng.Assert;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;
import login.Common;
import pageObjects.MembersArea;

public class ReservationProcess extends Common {
	
	Logger logger = Logger.getLogger("ReservationProcess.class");
	 MembersArea MA = new MembersArea();
	 
	 	String jsExecute = "arguments[0].scrollIntoView(true);"; 
	 	String Location; //Location
	 	String Resort; //Resort
	 	String RoomType; ///Room Type
	 	String Checkin; //Checkin Date
	 	String Checkout;// Checkout Date
	 	String Points; //Number of Points that cost
	 	String ResortHeader = "Resort:"; //Header
	 	String SpecialRequests = "Our goal is to make every effort to accommodate the accessibility needs of our guests."; //Special Request Text box
	 	String SpecialAccomodations = "Requested"; //Special Accommodations message
	 	String VacationGuard = "VacationGuard® Travel Club Plan *"; //VacationGuard® Travel Club Plan * header
	 	String LegalProtectionPlan = "Legal Protection Plan - a Travel Benefit"; //Legal Protection Plan - a Travel Benefit header
	 	String SkyMed = "SkyMed"; //SkyMed header
	 	String DialCare = "HEALTHIEST YOU - A TRAVEL BENEFIT"; //DialCare - A Travel Benefit Header
	 	String Miscellaneous = null;//
	 	String NoResFeesMessage = "No reservation fees are applicable at this time.";
	 	
	 	//Payment Options
	 	String PaymentOptionsValue = "N"; //Payment option value to match in FinalConfirmation() Method
	 	String MiscellaneousValue = "N"; //Miscellaneous value to match in FinalConfirmation() Method
	 	String Delinquency = "N"; //delinquency value to match in FinalConfirmation() Method
	 	String UsePoints = "200"; //Use points in payment options
	 	String EstRentPoints; //Points to use for Allotment in payment options
	 	String Remainder; //Remainder Value
	 	String TotalPointCost; //Total Point Cost
	 	
	 	//Final Confirmation
	 	String ReservationConfirmationHeader = "Reservation Confirmation"; //Reservation Confirmation Header
	 	String Email = "Tester@diamondresorts.com"; //Email
	 	String EmailMessage; //Email Message
	 	String ConfirmationNumber; //Confirmation Number
	 	
	 	//Points After Purchase
	 	String EstPointsAfterPurchase; //Estimation Points After Purchase
	 	String PointsAfterPurchase; //Points After Purchase
	 	
	 	
	 	
	 @Test(priority = 1)
	 public void SearchResults() throws InterruptedException {
		 
		 WebElement Results = driver.findElement(By.xpath("//*[@id='GridResultsTable']/tbody"));
		 List<WebElement> tr = Results.findElements(By.tagName("tr"));
			int rowsnum = tr.size();
	
			Search:
			for(int i = 1; i<=rowsnum; i++) {
				
				//Location
				Location = driver.findElement(By.xpath("//*[@id='GridResultsTable']/tbody/tr[" + String.valueOf(i) + "]/td[1]/p")).getText();
					logger.info("Location: "+Location);
					
				//Resort
			 	Resort = driver.findElement(By.xpath("//*[@id='GridResultsTable']/tbody/tr[" + String.valueOf(i) + "]/td[2]/p/a")).getText();
				 	logger.info("Resort: "+Resort);
				 	
			 	//RoomType
			 	RoomType = driver.findElement(By.xpath("//*[@id='GridResultsTable']/tbody/tr[" + String.valueOf(i) + "]/td[3]/p/a")).getText();
				 	logger.info("RoomType: "+RoomType);
				 	
			 	//Checkin
			 	Checkin = driver.findElement(By.xpath("//*[@id='GridResultsTable']/tbody/tr[" + String.valueOf(i) + "]/td[4]/p")).getText();
				 	logger.info("Checkin Date: "+Checkin);
				 	
			 	//Checkout
			 	Checkout = driver.findElement(By.xpath("//*[@id='GridResultsTable']/tbody/tr[" + String.valueOf(i) + "]/td[5]/p")).getText();
				 	logger.info("Checkout Date: "+Checkout);
				 	
			 	//Points
			 	Points = driver.findElement(By.xpath("//*[@id='GridResultsTable']/tbody/tr[" + String.valueOf(i) + "]/td[6]/p")).getText();
				 	logger.info("Points that cost the reservation: "+Points);
				 	
				 	jse.executeScript(jsExecute,  driver.findElement(By.xpath("//*[@id='GridResultsTable']/tbody/tr[" + String.valueOf(i) + "]/td[7]/div/p/a")));
				 	
			 	 //Click on Book It
				 driver.findElement(By.xpath("//*[@id='GridResultsTable']/tbody/tr[" + String.valueOf(i) + "]/td[7]/div/p/a")).click();
				 	logger.info("Clicked on Bookit");
				 	
				 	wait.until(ExpectedConditions.invisibilityOfElementLocated(MA.MembersArea_Loader)); //Element Disappears
				 	Thread.sleep(1000);
				 	
				 	//Resort not available
				 	if(driver.findElements(By.xpath("//button[text()='Ok']")).size()!=0) //Resort not available 
					{
				 		if(driver.findElement(By.xpath("//*[@id='alert-dlg']")).getText().substring(0,34).equals("In accordance with Club Guidelines")) {
				 			
				 			Points = String.valueOf(Integer.parseInt(Points)/2);
				 				logger.info(driver.findElement(By.xpath("//*[@id='alert-dlg']")).getText());
				 					driver.findElement(By.xpath("//button[text()='Ok']")).sendKeys(Keys.ENTER);
				 						logger.info("Clicked on Ok");
				 					
				 					 	//******************************Add Cookie************************************************//
				 						driver.manage().addCookie(new Cookie("Points",Points));
				 						driver.manage().addCookie(new Cookie("Resort",Resort));
				 						//******************************************************************************//
				 					 	
				 							break Search;
				 		}
				 		
				 		//Usage of Future Points
				 		else if(driver.findElement(By.xpath("//*[@id='alert-dlg']")).getText().substring(0,78).equals("In order to book this reservation, points will be taken from your future usage")) {
				 			
				 			driver.findElement(By.xpath("//button[text()='Ok']")).sendKeys(Keys.ENTER);
	 							logger.info("Clicked on Ok");
	 							
	 							break Search;
				 		
				 		}
				 		
				 		//Resort is not available (Middle Tier)
				 		else {
				 			
				 			driver.findElement(By.xpath("//button[text()='Ok']")).sendKeys(Keys.ENTER);
				 				logger.info("Unfortunely selected resort is unavabilable - Clicked on OK button");
				 		}
				 		
					}
					
					else
					{

					 	//******************************Add Cookie************************************************//
						driver.manage().addCookie(new Cookie("Points",Points));
						driver.manage().addCookie(new Cookie("Resort",Resort));
						//******************************************************************************//
					 	
						break Search;
					}
			
				
			}
 
	 }
	 
	 @Test(priority = 2)
	 @Parameters("Delinquency")
	 public void Step2_ResortAndGuestDetails_ReviewReservation(String Delinquency) {
	 
		wait.until(ExpectedConditions.textToBe(MA.ReservationSummary_ResortHeader, ResortHeader)); //Text Is Present
		
			//-------------------------------------------Review Your Reservation----------------------------------------//
		
		 //Reservation Summary
		 
		 //Resort
		//Assert.assertEquals(driver.findElement(MA.ReservationSummary_Resort).getText().toUpperCase(), Resort.replace("'", "").toUpperCase().trim());
		 	logger.info("Resort Name: "+driver.findElement(MA.ReservationSummary_Resort).getText());
		 	
		 //Room Type
	 	 Assert.assertEquals(driver.findElement(MA.ReservationSummary_RoomType).getText().replaceAll(" ", "").trim(), RoomType.replaceAll(" ", "").trim());
		 	logger.info("Room Type Name Matched: "+driver.findElement(MA.ReservationSummary_RoomType).getText());	
		 	
	 	//Checkin Date
	 	 Assert.assertEquals(driver.findElement(MA.ReservationSummary_Checkin).getText().toUpperCase(), Checkin.toUpperCase());
		 	logger.info("Checkin Date  Matched: "+driver.findElement(MA.ReservationSummary_Checkin).getText());	
			 	
	 	//Checkout
	 	 Assert.assertEquals(driver.findElement(MA.ReservationSummary_Checkout).getText().toUpperCase(), Checkout.toUpperCase());
		 	logger.info("Checkout Date Matched: "+driver.findElement(MA.ReservationSummary_Checkout).getText());	
		 	
	 	//Points
	 	 Assert.assertEquals(driver.findElement(MA.ReservationSummary_Points).getText(), Points);
		 	logger.info("Points Matched: "+driver.findElement(MA.ReservationSummary_Points).getText());	
		 	
		 //Total Payments
	 	if(Delinquency.equals("Y")) {
	 		Assert.assertEquals(driver.findElement(MA.TotalPayments_Delinquency).getText().replaceAll("USD", "").trim(), driver.manage().getCookieNamed("TotalPayments").getValue());
	 			logger.info("Total Payments Matched: "+driver.findElement(MA.TotalPayments_Delinquency).getText());
	 	}
	 	
	 	else {
	 	Assert.assertEquals(driver.findElement(MA.TotalPayments).getText().replaceAll("USD", "").trim(), driver.manage().getCookieNamed("TotalPayments").getValue());
	 		logger.info("Total Payments Matched: "+driver.findElement(MA.TotalPayments).getText());
	 	}
		 	
		 	jse.executeScript(jsExecute,  driver.findElement(MA.Next_Button)); //Scroll to Element Location
		 	
		 //Next Button
		 driver.findElement(MA.Next_Button).click();
		 	logger.info("CLicked on Next Button - Step2 Review your Reservation");
		 	
	 }
	 
	
	 @Test(priority = 3)
	 public void Step2_ResortAndGuestDetails_With_RequestsandAccessibility() {
		 	
	 	wait.until(ExpectedConditions.textToBe(MA.ReservationSummary_ResortHeader, ResortHeader)); //Text Is Present

	 	//------------------------------------------------------Requests and Accessibility-------------------------------------------------------------------------//
	 	
	 	//Special Request
	 	driver.findElement(MA.Requests).sendKeys(SpecialRequests);
	 		logger.info("Entered Special Request");
	 		
 		jse.executeScript(jsExecute,  driver.findElement(MA.WheelChair));	//Scroll to Element Location

	 	//Hearing Impairment
	 	driver.findElement(MA.HearingImpairment).click();
	 		logger.info("Clicked on - Hearing Impairment");
	 		
	 		
	 	//Partial Mobility Impairment
	 	driver.findElement(MA.Partial_Mobility_Impairment).click();
	 		logger.info("Clicked on - Partial Mobility Impairment");
	 		
	 		
	 	//Visual Impairment
	 	driver.findElement(MA.Visual_Impairment).click();
	 		logger.info("Clicked on - Visual Impairment");
	 		
	 		
	 	//Wheel Chair
	 	driver.findElement(MA.WheelChair).click();
	 		logger.info("Clicked on - Wheel Chair");
	 		
	 		
	 	//Pre-Existing Medical Condition
	 	driver.findElement(MA.MedicalCondition).click();
	 		logger.info("Clicked on Pre-Existing Medical Condition");
	 		
	 		
	 	//Uses Service Dog
	 	driver.findElement(MA.ServiceDog).click();
	 		logger.info("Clicked on - Uses Service Dog");
	 		
 		jse.executeScript(jsExecute,  driver.findElement(MA.HearingImpairment));	//Scroll to Element Location	
	 		
	 	//Disability or require accessibility Assistance
	 	driver.findElement(MA.Disability_Assistance).sendKeys(SpecialRequests);
	 		logger.info("Entered Disability or require accessibility Assistance");
	 		
	 		
	 	//Click on Next
	 	driver.findElement(MA.ReqAcc_Next).click();
	 		logger.info("Clicked on Next - Step2 Requests and Accessibility");
	 		
	 }
	 
	//--------------------------------------------------------Guest Information-----------------------------------------------------------------------------------//
	 
	 
	@Test(priority = 4)
	public void Step2_ResortAndGuestDetails_Without_RequestsandAccessibility() throws InterruptedException {
		
		wait.until(ExpectedConditions.textToBe(MA.ReservationSummary_ResortHeader, ResortHeader)); //Text Is Present
		
		Thread.sleep(1000);
			
		jse.executeScript(jsExecute,  driver.findElement(MA.ReqAcc_Next));	//Scroll to Element Location	
	
		//Click on Next
	 	driver.findElement(MA.ReqAcc_Next).click();
	 		logger.info("Clicked on Next - Step2 Requests and Accessibility");
	
	}
	
	@Test(priority = 5)
	public void Step2_ResortAndGuestDetails_GuestInformation() throws InterruptedException {

	 	//-------------------------------------------Guest Information-----------------------------------------------//
 		
 		wait.until(ExpectedConditions.textToBe(MA.ReservationSummary_ResortHeader, ResortHeader)); //Text Is Present
 		wait.until(ExpectedConditions.elementToBeClickable(MA.GuestInfo_Next)); //Text Is Present
 		
 		Thread.sleep(1000);
 		
 		//Special Accomodations
 		if(driver.findElements(MA.SpecialAccommodations).size()!=0) {
		 Assert.assertEquals(driver.findElement(MA.SpecialAccommodations).getText(), SpecialAccomodations);
		 	logger.info("Special Accommodations Matched: "+driver.findElement(MA.SpecialAccommodations).getText());
 		}
 		
 		else {
 			
 			logger.info("No Special Request");
 		}
 		
 		if(driver.findElement(MA.GuestInfo_Member_Myself).isEnabled()) {
			
			jse.executeScript("arguments[0].scrollIntoView(true);",  driver.findElement(MA.GuestInfo_Next));	//Scroll to Element Location
			
			//Click on Next
		 	driver.findElement(MA.GuestInfo_Next).click();
		 		logger.info("Clicked on Next - Step2 Guest Information");
		}
		
		else {
			
			NewGuest();
		
		}
		 	
	}
	 
	 @Test(priority = 6)
	 public void Step3_AdditionalOffers_TravelProtection() throws InterruptedException { 
		 
	//-----------------------------------------------Travel Protection-------------------------------------------------------//
		 
		 wait.until(ExpectedConditions.textToBe(MA.ReservationSummary_ResortHeader, ResortHeader)); //Text Is Present
		 wait.until(ExpectedConditions.elementToBeClickable(MA.TravelProtection_Next)); //Text Is Present
		 
		 Thread.sleep(1000);
		 
		 jse.executeScript(jsExecute,  driver.findElement(MA.TravelProtection_Next));	//Scroll to Element Location

		 //Terms
		 if(driver.findElements(MA.RPP_Message).size()!=0) {
			 
			 logger.info(driver.findElement(MA.RPP_Message).getText());
			 
		 }
		 
		 else{
			 
			 driver.findElement(MA.TermsConditons).click();
			 	logger.info("Clicked on - I have read and understand the terms described above");
		 }
		 	
		 	
		 	
		 //Next button
		 driver.findElement(MA.TravelProtection_Next).click();
		 	logger.info("Clicked on Travel Protection - Next Button");

	 } 	
	
	 @Test(priority = 7)
		public void Step3_AdditionalOffers_Miscellaneous_Purchase() throws InterruptedException {
		 
		wait.until(ExpectedConditions.textToBe(MA.ReservationSummary_ResortHeader, ResortHeader)); //Text Is Present
		
		 Thread.sleep(1000);
		 
		 jse.executeScript(jsExecute,  driver.findElement(MA.Miscellaneous_Next)); //Scroll to Element Location
		 
		 Thread.sleep(1000);
		 
		 //click on Next
		 driver.findElement(MA.Miscellaneous_Next).click();
		 	logger.info("Clicked on Next");
		 	
		 
	 }
	 //------------------------------------------------Miscellaneous---------------------------------------------------------------//
		 	
	@Test(priority = 8)
	public void Step3_AdditionalOffers_Miscellaneous_DeclinePurchase() throws InterruptedException {
		
		wait.until(ExpectedConditions.textToBe(MA.LegalProtectionPlan_Header, LegalProtectionPlan.toUpperCase())); //Text Is Present
		Thread.sleep(2000);
		
 		Miscellaneous = "N"; //for step4 process
		
		//Vacation Guard Travel Club Plan
		 Assert.assertEquals(driver.findElement(MA.VacationGuard_Header).getText().toUpperCase(), VacationGuard.toUpperCase());
		 	logger.info("VACATIONGUARD® TRAVEL CLUB PLAN * header Matched: "+driver.findElement(MA.VacationGuard_Header).getText());
		 	
		 	//Decline Vacation Guard
		 	driver.findElement(MA.VacationGuard_Decline).click();
		 		logger.info("Declined Vacation Guard");
		 		
		 		jse.executeScript(jsExecute,  driver.findElement(MA.LegalProtectionPlan_Decline)); //Scroll to Element Location
		 		
		 
		//Legal Protection Plan
		 Assert.assertEquals(driver.findElement(MA.LegalProtectionPlan_Header).getText().toUpperCase(), LegalProtectionPlan.toUpperCase());
		 	logger.info("LEGAL PROTECTION PLAN - A TRAVEL BENEFIT header Matched: "+driver.findElement(MA.LegalProtectionPlan_Header).getText());
		 	
		 	//Decline Legal Protection Plan
		 	driver.findElement(MA.LegalProtectionPlan_Decline).click();
		 		logger.info("Declined Legal Protection Plan");
		 		
		 		Thread.sleep(1000);
		 		
		 		jse.executeScript(jsExecute,  driver.findElement(MA.Skymed_Decline)); //Scroll to Element Location
		 		
		//Skymed
		 Assert.assertEquals(driver.findElement(MA.Skymed_Header).getText().toUpperCase(), SkyMed.toUpperCase());
		 	logger.info("SKYMED header Matched: "+driver.findElement(MA.Skymed_Header).getText());
		 	
		 	//Decline Skymed
		 	driver.findElement(MA.Skymed_Decline).click();
		 		logger.info("Declined SkyMed");	
		 		
		 		Thread.sleep(1000);
		 		
		 		jse.executeScript(jsExecute,  driver.findElement(MA.DialCare_Decline)); //Scroll to Element Location
		
		//DialCare - A Travel Benefit
		Assert.assertEquals(driver.findElement(MA.DialCare_Header).getText().toUpperCase(), DialCare.toUpperCase());
	 		logger.info("DIALCARE - A TRAVEL BENEFIT header Matched: "+driver.findElement(MA.DialCare_Header).getText());
			 	
		 	//Decline DialCare
		 	driver.findElement(MA.DialCare_Decline).click();
		 		logger.info("Declined DialCare");	
		 		
				 		
		 //click on Next
		 driver.findElement(MA.Miscellaneous_Next).click();
		 	logger.info("Clicked on Next");		
	 		
	}
	
	@Test(priority=9)
	public void Step4_Payments_WithPaymentOptions() throws InterruptedException {
		
		wait.until(ExpectedConditions.textToBe(MA.ReservationSummary_ResortHeader, ResortHeader)); //Text Is Present
		
		Payment_Options(); //Payment Options
		
		wait.until(ExpectedConditions.elementToBeClickable(MA.PaymentOptions)); //Payemnt Options
		
		//TotalPayments Points Rent Comparision
		Assert.assertEquals(EstRentPoints, driver.findElement(MA.Step4_TotalPayments_RentPointsCost).getText().replaceAll("[^0-9]",""));
			logger.info("Rent Points Matched in Total Payments Section: "+driver.findElement(MA.Step4_TotalPayments_RentPointsCost).getText().replaceAll("[^0-9]",""));
			
		//TotalPayments Points Rent USD Comparision
		Assert.assertEquals(TotalPointCost.replace(" ", "").trim(), driver.findElement(MA.Step4_TotalPayments_RentPointsCostUSD).getText().replaceAll(" ", "").trim());
			logger.info("Cost for Renting Points Matched in Total Payments Section: "+driver.findElement(MA.Step4_TotalPayments_RentPointsCostUSD).getText());
			
			AddCreditCard(); //Add Credit Card
			
			Thread.sleep(1000);
			
			jse.executeScript(jsExecute,  driver.findElement(MA.Confirm_Reservation)); //Scroll to Element Location
			
				
		//Credit Card Approval Check Box
		if(driver.findElements(MA.Creditcard_Terms).size()!=0) {
			
			driver.findElement(MA.Creditcard_Terms).click();
				logger.info("Clicked on Authorize Credit Card Check Box");
		}
			
			
		//Authorize Payment
		driver.findElement(MA.AuthorizePayment).click();
			logger.info("Clicked on Authorize Payment");
		
		//Next
		driver.findElement(MA.Confirm_Reservation).click();
			logger.info("Clicked on to Confirm Reservation");
		
	}
	
	@Test(priority=10)
	public void Step4_Payments_WithoutCCPayment() throws InterruptedException {
		
		wait.until(ExpectedConditions.textToBe(MA.ReservationSummary_ResortHeader, ResortHeader)); //Text Is Present
		
		jse.executeScript(jsExecute,  driver.findElement(MA.Confirm_Reservation)); //Scroll to Element Location
		
		driver.findElement(MA.Confirm_Reservation).click();
			logger.info("Clicked on to Confirm Reservation");
				
	
	}
	
	@Test(priority=11)
	public void Step4_Payments_MiscellaneousPurchase() throws InterruptedException {
		
		MiscellaneousValue = "Y";
		
		wait.until(ExpectedConditions.textToBe(MA.ReservationSummary_ResortHeader, ResortHeader)); //Text Is Present
		
		
		//Total Payments - Legal Protec, Sky Med, Dial Care
		for(int i = 1; i<=6; i+=2) {
				
			logger.info(driver.findElement(By.xpath("//*[@id='content-right']/div/div[6]/div[" +String.valueOf(i)+ "]")).getText() + ":" +driver.findElement(By.xpath("//*[@id='content-right']/div/div[6]/div[" +String.valueOf(i+1)+ "]")).getText());
				
		 	//******************************Add Cookie************************************************//
			driver.manage().addCookie(new Cookie(driver.findElement(By.xpath("//*[@id='content-right']/div/div[6]/div[" +String.valueOf(i)+ "]")).getText(), driver.findElement(By.xpath("//*[@id='content-right']/div/div[6]/div[" +String.valueOf(i+1)+ "]")).getText()));
			//***************************************************************************************//
			
			
		}
	
		//Total**
		logger.info(driver.findElement(By.xpath("//*[@id='content-right']/div/div[6]/div[9]/div[1]")).getText() + ":" +driver.findElement(By.xpath("//*[@id='content-right']/div/div[6]/div[9]/div[2]")).getText());
		
			
			AddCreditCard(); //Add Credit Card
			
			Thread.sleep(1000);
			
			jse.executeScript(jsExecute,  driver.findElement(MA.Confirm_Reservation)); //Scroll to Element Location
			
				
			//Credit Card Approval Check Box
			if(driver.findElements(MA.Creditcard_Terms).size()!=0) {
				
				driver.findElement(MA.Creditcard_Terms).click();
					logger.info("Clicked on Authorize Credit Card Check Box");
			}
				
			
		//Authorize Payment
		driver.findElement(MA.AuthorizePayment).click();
			logger.info("Clicked on Authorize Payment");
		
		//Next
		driver.findElement(MA.Confirm_Reservation).click();
			logger.info("Clicked on to Confirm Reservation");
		
		
	}
	
	@Test(priority=11)
	public void Step4_Payments_Delinquency() throws InterruptedException {
		
		wait.until(ExpectedConditions.textToBe(MA.ReservationSummary_ResortHeader, ResortHeader)); //Text Is Present
		
		Assert.assertEquals(driver.findElement(MA.TotalPayments_Delinquency).getText().replaceAll("USD", "").trim(), driver.manage().getCookieNamed("TotalPayments").getValue());
			logger.info("Total Payments Matched in Step4 Payments: "+driver.findElement(MA.TotalPayments_Delinquency).getText());
		
		AddCreditCard(); //Add Credit Card
			
		Thread.sleep(1000);
			
		jse.executeScript(jsExecute,  driver.findElement(MA.Confirm_Reservation)); //Scroll to Element Location
			
		//Authorize Payment
		driver.findElement(MA.AuthorizePayment).click();
			logger.info("Clicked on Authorize Payment");
		
		//Next
		driver.findElement(MA.Confirm_Reservation).click();
			logger.info("Clicked on to Confirm Reservation");
		
		
	}
	
	@Test(priority=12)
	public void FinalConfirmation() {
		
		wait.until(ExpectedConditions.elementToBeClickable(MA.FinalConfirm_SendEmail)); //Send Email
		
		//Reservation Confirmation
		Assert.assertEquals(driver.findElement(MA.FinalConfirm_ReservationConfirmation_Header).getText(), ReservationConfirmationHeader);
	 		logger.info("Reservation Confirmation header Matched: "+driver.findElement(MA.FinalConfirm_ReservationConfirmation_Header).getText());
	 		
	 	//Send Email
	 	driver.findElement(MA.FinalConfirm_SendEmail).sendKeys(Email);
	 		logger.info("Entered Email Address");
	 	
	 	//Send Email Button
	 	driver.findElement(MA.FinalConfirm_SendEmail_Button).click();
	 		logger.info("Clicked on Send Email Button");
	 		
 		wait.until(ExpectedConditions.visibilityOfElementLocated(MA.FinalConfirm_SendEmail_SuccessMes)); //Text Is Present
 		
 		EmailMessage = driver.findElement(MA.FinalConfirm_SendEmail_SuccessMes).getText();
 			logger.info(EmailMessage);
 			
 			
 			//-------------------------------------------------Stay Information----------------------------------------------------//
 			
 			//With Payment Option
 			if(PaymentOptionsValue.equals("Y")) {
 				
 				//Estimate Points After Purchase
				EstPointsAfterPurchase = String.valueOf(Integer.parseInt(driver.manage().getCookieNamed("PointsBeforePurchase").getValue()) - Integer.parseInt(UsePoints));
					logger.info("Points After Purchase Should be: "+EstPointsAfterPurchase);
					
					//Points After Purchase
					PointsAfterPurchase = driver.findElement(MA.MembesArea_AvailablePoints).getText();
						Assert.assertEquals(PointsAfterPurchase, EstPointsAfterPurchase);
							logger.info("Points Matched: "+PointsAfterPurchase);
 				
 				for(int i = 2; i<=23; i+=3) {
 	 				
 					logger.info(driver.findElement(By.xpath("//*[@id='book-content-left']/div[2]/div[3]/div/div[3]/div[" +String.valueOf(i) + "]/div/label")).getText() + ":" +driver.findElement(By.xpath("//*[@id='book-content-left']/div[2]/div[3]/div/div[3]/div[" +String.valueOf(i+1) + "]/div")).getText());
 					
				 	//******************************Add Cookie************************************************//
					driver.manage().addCookie(new Cookie(driver.findElement(By.xpath("//*[@id='book-content-left']/div[2]/div[3]/div/div[3]/div[" +String.valueOf(i) + "]/div/label")).getText(),driver.findElement(By.xpath("//*[@id='book-content-left']/div[2]/div[3]/div/div[3]/div[" +String.valueOf(i+1) + "]/div")).getText()));
					//******************************************************************************//
 				}
			
 				//Rent Points Cost
 				Assert.assertEquals(TotalPointCost.replace(" ", "").trim(), driver.manage().getCookieNamed("Diamond Value Administrative Fee(Non-Refundable):").getValue().replaceAll(" ", "").trim());
 					logger.info("Diamond Value Administrative Fee(Non-Refundable): Matched: "+ driver.manage().getCookieNamed("Diamond Value Administrative Fee(Non-Refundable):").getValue());
					
			}
 			
 			else if(MiscellaneousValue.equals("Y")) {
 				
 				//Estimate Points After Purchase
				EstPointsAfterPurchase = String.valueOf(Integer.parseInt(driver.manage().getCookieNamed("PointsBeforePurchase").getValue()) - Integer.parseInt(driver.manage().getCookieNamed("Points").getValue()));
					logger.info("Points After Purchase Should be: "+EstPointsAfterPurchase);
					
					//Points After Purchase
					PointsAfterPurchase = driver.findElement(MA.MembesArea_AvailablePoints).getText();
						Assert.assertEquals(PointsAfterPurchase, EstPointsAfterPurchase);
							logger.info("Points Matched: "+PointsAfterPurchase);
 				
 				for(int i = 2; i<=32; i+=3) {
 	 				
 					logger.info(driver.findElement(By.xpath("//*[@id='book-content-left']/div[2]/div[3]/div/div[3]/div[" +String.valueOf(i) + "]/div/label")).getText() + ":" +driver.findElement(By.xpath("//*[@id='book-content-left']/div[2]/div[3]/div/div[3]/div[" +String.valueOf(i+1) + "]/div")).getText());
 					
				 	//******************************Add Cookie************************************************//
					driver.manage().addCookie(new Cookie(driver.findElement(By.xpath("//*[@id='book-content-left']/div[2]/div[3]/div/div[3]/div[" +String.valueOf(i) + "]/div/label")).getText(),driver.findElement(By.xpath("//*[@id='book-content-left']/div[2]/div[3]/div/div[3]/div[" +String.valueOf(i+1) + "]/div")).getText()));
					//******************************************************************************//
 				}
 				
 			/*	//Healthiest you Compare from step4 to final confirmation
 				Assert.assertEquals(driver.manage().getCookieNamed("DialCare:").getValue().replaceAll(" ", "").trim(), driver.manage().getCookieNamed("Healthiest You(Non-Refundable):").getValue().replaceAll(" ", "").trim());
					logger.info("Healthiest you Matched");*/
					
				//Legal Protection Plan Compare from step4 to final confirmation
 				Assert.assertEquals(driver.manage().getCookieNamed("Legal Protection:").getValue().replaceAll(" ", "").trim(), driver.manage().getCookieNamed("Legal Protection Plan(Non-Refundable):").getValue().replaceAll(" ", "").trim());
					logger.info("Legal Protection Plan Matched");
					
				/*//VacationGuard Travel Protection Compare from step4 to final confirmation
 				Assert.assertEquals(driver.manage().getCookieNamed("VacationGuard® Travel Club Plan").getValue().replaceAll(" ", "").trim(), driver.manage().getCookieNamed("VacationGuard Travel Protection:").getValue().replaceAll(" ", "").trim());
					logger.info("VacationGuard Travel Protection  Matched");*/
					
				//SkyMed Individual Compare from step4 to final confirmation
 				Assert.assertEquals(driver.manage().getCookieNamed("SkyMed Individual:").getValue().replaceAll(" ", "").trim(), driver.manage().getCookieNamed("SkyMed Individual Plan(Non-Refundable):").getValue().replaceAll(" ", "").trim());
					logger.info("SkyMed Matched");
 				
 				
 			}
 			
 			else {

 				//Estimate Points After Purchase
				EstPointsAfterPurchase = String.valueOf(Integer.parseInt(driver.manage().getCookieNamed("PointsBeforePurchase").getValue()) - Integer.parseInt(driver.manage().getCookieNamed("Points").getValue()));
					logger.info("Points After Purchase Should be: "+EstPointsAfterPurchase);
					
					//Points After Purchase
					PointsAfterPurchase = driver.findElement(MA.MembesArea_AvailablePoints).getText();
						Assert.assertEquals(PointsAfterPurchase, EstPointsAfterPurchase);
							logger.info("Points Matched: "+PointsAfterPurchase);
							
 				for(int i = 2; i<=20; i+=3) {
 				
					logger.info(driver.findElement(By.xpath("//*[@id='book-content-left']/div[2]/div[3]/div/div[3]/div[" +String.valueOf(i) + "]/div/label")).getText() + ":" +driver.findElement(By.xpath("//*[@id='book-content-left']/div[2]/div[3]/div/div[3]/div[" +String.valueOf(i+1) + "]/div")).getText());
 					
				 	//******************************Add Cookie************************************************//
					driver.manage().addCookie(new Cookie(driver.findElement(By.xpath("//*[@id='book-content-left']/div[2]/div[3]/div/div[3]/div[" +String.valueOf(i) + "]/div/label")).getText(),driver.findElement(By.xpath("//*[@id='book-content-left']/div[2]/div[3]/div/div[3]/div[" +String.valueOf(i+1) + "]/div")).getText()));
					//******************************************************************************//
				}
 			}

 				//CheckIn Date Compare
 				Assert.assertEquals(Checkin.toUpperCase(), driver.manage().getCookieNamed("Check-In Date:").getValue().toUpperCase());
 					logger.info("Check In Date Matched: "+ driver.manage().getCookieNamed("Check-In Date:").getValue().toUpperCase());
 					
				//Checkout Date Compare
 				Assert.assertEquals(Checkout.toUpperCase(), driver.manage().getCookieNamed("Check-Out Date:").getValue().toUpperCase());
 					logger.info("Check Out Date Matched: "+ driver.manage().getCookieNamed("Check-Out Date:").getValue().toUpperCase());
 					
 				//Total Points Used
 				Assert.assertEquals(Points, driver.manage().getCookieNamed("Total Points Used:").getValue());
 					logger.info("Total Points Used Matched: "+ driver.manage().getCookieNamed("Total Points Used:").getValue());
 					
 				
	}
		
		
	public void Payment_Options() throws InterruptedException {
		
		PaymentOptionsValue = "Y"; //Set the value to Y for Assertion
		
		//Payment Options
		driver.findElement(MA.PaymentOptions).click();
			logger.info("Clicked on Payment Options Button");
			
			wait.until(ExpectedConditions.textToBe(MA.ReservationSummary_ResortHeader, ResortHeader)); //Text Is Present
			
		 //Number of points 
		Assert.assertEquals(driver.findElement(MA.PaymentOptions_NumofPoints).getAttribute("value"), Points);
			logger.info("Points Matched: "+driver.findElement(MA.PaymentOptions_NumofPoints).getAttribute("value"));
			
			
		//Available
		Assert.assertEquals(driver.findElement(MA.PaymentOptions_AvailPoints).getText(), driver.manage().getCookieNamed("PointsBeforePurchase").getValue());
			logger.info("Available Points Matched: "+driver.findElement(MA.PaymentOptions_AvailPoints).getText());
			
		//Number of Points to use - Allotment
		EstRentPoints = String.valueOf(Integer.parseInt(Points) - Integer.parseInt(UsePoints)); 
			logger.info("Estimated Rent Points: "+EstRentPoints);
		
			driver.findElement(MA.PaymentOptions_Allotment).click();
				Thread.sleep(1000);
					driver.findElement(MA.PaymentOptions_Allotment).sendKeys(UsePoints);
						logger.info("Entered Allotment Points: "+UsePoints);
						
		//Rent Points
		driver.findElement(MA.PaymentOptions_RentPoints).click();
			Thread.sleep(1000);
			
			//Estimated Rent Points
			Remainder =  (String) jse.executeScript("return arguments[0].value;", driver.findElement(MA.PaymentOptions_RentPoints_Remainder));
				logger.info("Remainder Points: "+Remainder);
			
			Assert.assertEquals(Remainder, EstRentPoints);
				logger.info("Remainder Points Matched: "+Remainder);
			
				driver.findElement(MA.PaymentOptions_RentPoints).sendKeys(EstRentPoints);
					logger.info("Entered Rent Points: "+Remainder);
					Thread.sleep(1000);
					
				//Click on Refresh
				driver.findElement(MA.PaymentOptions_Refresh).click();
					logger.info("Clicked on Refresh");
					
					wait.until(ExpectedConditions.elementToBeClickable(MA.PaymentOptions_Next)); //Next is Clickable
					
					//Remainder value after Refresh
					Remainder =  (String) jse.executeScript("return arguments[0].value;", driver.findElement(MA.PaymentOptions_RentPoints_Remainder));
						logger.info("Remainder Points: "+Remainder);
					
					if(Remainder.equals("0")) {
						
						//Total
						TotalPointCost = driver.findElement(MA.PayemntOptions_Total).getAttribute("value");
							logger.info("Total Cost to Rent Points: "+TotalPointCost);
						
						//Click on Next
						driver.findElement(MA.PaymentOptions_Next).click();
							logger.info("Clicked on NEXT"); 
					}
					
					else {
						
						logger.info("Remainder is not Zero(0)");
						
					}
	
	}
	
	
	public void AddCreditCard() throws InterruptedException {
		
		if(driver.findElements(MA.SelectCard).size()!=0) {
			
			//Select Card
			driver.findElement(MA.SelectCard).click();
				logger.info("Selected Credit Card");
				
			//Enter CVV
			driver.findElement(MA.CVV).sendKeys(Cvv);
				logger.info("Entered Cvv");
			
		}
		
		else {
			
		//AddCard
		driver.findElement(MA.AddCard).click();
			logger.info("Clicked on Add Card");
			
			wait.until(ExpectedConditions.elementToBeClickable(MA.AddButton)); //Add Button
			
			jse.executeScript(jsExecute,  driver.findElement(MA.AddButton)); //Scroll to Element Location
		
		//Card Number
		driver.findElement(MA.CardNumber).sendKeys(CCNumber);
			logger.info("Entered Credit Card Number: "+CCNumber);
			
		//Card Name
		driver.findElement(MA.CardHolderName).sendKeys(CCName);
			logger.info("Entered Credit Card HOlder Name: "+CCName);
			
		//Expiration Date
		driver.findElement(MA.ExpYear).click();
			new Select(driver.findElement(MA.ExpYear)).selectByValue(ExpYr);// Exp Year;
				logger.info("Entered Exp Month and Year");
				
		//Click on Add
		driver.findElement(MA.AddButton).click();
			logger.info("Clicked on Add");
			
		Thread.sleep(1000);
			
		wait.until(ExpectedConditions.elementToBeClickable(MA.SelectCard)); //Select Credit Card
		
		jse.executeScript(jsExecute,  driver.findElement(MA.SelectCard)); //Scroll to Element Location
		
		//Select Card
		driver.findElement(MA.SelectCard).click();
			logger.info("Selected Credit Card");
			
		//Enter CVV
		driver.findElement(MA.CVV).sendKeys(Cvv);
			logger.info("Entered Cvv");
		
		
		}
	} 
	
	public void NewGuest() throws InterruptedException {
		
				
				//Guests
				driver.findElement(MA.GuestInfo_Guest).click();
					logger.info("Clicked on Guests");
					
				//New Guest
				driver.findElement(MA.GuestInfo_NewGuest).click();
					logger.info("Clicked on New Guest");
					
					//Save to Cookie Guest Charge
					String GuestCharge = driver.findElement(MA.GuestInfo_GuestCharge).getText();
						logger.info("Reservation Confirmation number is: "+GuestCharge.replaceAll("[^0-9^.^$]","")); //Stores only Numbers from the confirmation message);
						
						//*********************************************************************//
							driver.manage().addCookie(new Cookie("GuestCharge",GuestCharge.replaceAll("[^0-9^.^$]","")));
						//*********************************************************************//
							
				wait.until(ExpectedConditions.elementToBeClickable(MA.GuestInfo_FirstName)); //Element to be clickable
				Thread.sleep(1000);
				
				//FirstName
				driver.findElement(MA.GuestInfo_FirstName).sendKeys(FirstName);
					logger.info("Entered FirstName: "+FirstName);
					
				//LastName
				driver.findElement(MA.GuestInfo_LastName).sendKeys(LastName);
					logger.info("Entered LastName: "+LastName);
					
				//Address
				driver.findElement(MA.GuestInfo_Address).sendKeys(Address);
					logger.info("Entered Address: "+Address);
					
				//Country
				new Select(driver.findElement(MA.GuestInfo_Country)).selectByValue(Country);
					logger.info("Selected Country: "+Country);
					
				//PostalCode
				driver.findElement(MA.GuestInfo_PostalCode).sendKeys(PostalCode);
					logger.info("Entered PostalCode: "+PostalCode);
					
				//City/Town
				driver.findElement(MA.GuestInfo_CityTown).sendKeys(CityTown);
					logger.info("Entered City/Town: "+CityTown);
					
				//State
				new Select(driver.findElement(MA.GuestInfo_State)).selectByValue(State);
					logger.info("Selected State: "+State);
					
				//Email
				driver.findElement(MA.GuestInfo_Email).sendKeys(Common.Email);
					logger.info("Entered Email: "+Email);
				
				//Verify Email
				driver.findElement(MA.GuestInfo_VerifyEmail).sendKeys(Common.Email);
					logger.info("Entered Verify Email: "+Email);
					
				//HomePhone
				driver.findElement(MA.GuestInfo_HomePhone).sendKeys(HomePhone);
					logger.info("Entered HomePhone: "+HomePhone);
					
					jse.executeScript("arguments[0].scrollIntoView(true);",  driver.findElement(MA.GuestInfo_Next));	//Scroll to Element Location
					
					//Click on Next
				 	driver.findElement(MA.GuestInfo_Next).click();
				 		logger.info("Clicked on Next - Step2 Guest Information");
		
	}


}
