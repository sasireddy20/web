package membersArea_Reservations;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.Cookie;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.testng.Assert;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;


import login.Common;
import pageObjects.MembersArea;

public class Reservation_NewGuest extends Common {
	
	
	String ResortHeader = "Resort:"; //Header
	String GuestCharge; //Guest Charge
	Logger logger = Logger.getLogger("ReservationProcess.class");
	MembersArea MA = new MembersArea();
	ReservationProcess ResProcess = new ReservationProcess();
	
	@Test
	@Parameters("Delinquency")
	public void Step2GuestInformation_NewGuest(String Delinquency) throws InterruptedException {
		
		
		ResProcess.SearchResults();
		ResProcess.Step2_ResortAndGuestDetails_ReviewReservation(Delinquency);
		ResProcess.Step2_ResortAndGuestDetails_Without_RequestsandAccessibility();
		
		
		wait.until(ExpectedConditions.textToBe(MA.ReservationSummary_ResortHeader, ResortHeader)); //Text Is Present
		
		if(driver.findElement(MA.GuestInfo_Guest).isEnabled()) {
			
			Thread.sleep(1000);
			
			ResProcess.NewGuest();
			
			
		}
		
		else {
			
			Thread.sleep(1000);
			
			jse.executeScript("arguments[0].scrollIntoView(true);",  driver.findElement(MA.GuestInfo_Next));	//Scroll to Element Location
			
			//Click on Next
		 	driver.findElement(MA.GuestInfo_Next).click();
		 		logger.info("Clicked on Next - Step2 Guest Information");
		
		}
		 		
		 ResProcess.Step3_AdditionalOffers_TravelProtection();
		 ResProcess.Step3_AdditionalOffers_Miscellaneous_DeclinePurchase();
		 
		 wait.until(ExpectedConditions.textToBe(MA.ReservationSummary_ResortHeader, ResortHeader)); //Text Is Present
		 
		 if(driver.findElement(MA.Step4_TotalPayments).getText().replaceAll("USD", "").trim().equals("$0.00")) {
			 
			 ResProcess.Step4_Payments_WithoutCCPayment();
		 }
		 
		 else {
			 
			 Assert.assertEquals(driver.manage().getCookieNamed("GuestCharge").getValue(),driver.findElement(MA.Step4_TotalPayments).getText().replaceAll("USD", "")); //Member(Myself) button should be disabled
				logger.info("GUest Charge PAssed with total payments");
			 
			 ResProcess.AddCreditCard(); //Add Credit Card
				
				Thread.sleep(1000);
				
				jse.executeScript("arguments[0].scrollIntoView(true);",  driver.findElement(MA.Confirm_Reservation)); //Scroll to Element Location
				
					
			//Credit Card Approval Check Box
			driver.findElement(MA.Creditcard_Terms).click();
				logger.info("Clicked on Authorize Credit Card Check Box");
				
			//Authorize Payment
			driver.findElement(MA.AuthorizePayment).click();
				logger.info("Clicked on Authorize Payment");
			
			//Next
			driver.findElement(MA.Confirm_Reservation).click();
				logger.info("Clicked on to Confirm Reservation");
			 
		 }
		 
		 //Final Confirmation Test Case
		 ResProcess.FinalConfirmation();
		 
		 jse.executeScript("arguments[0].scrollIntoView(true);",  driver.findElement(MA.FinalConfirm_ChangeGuestInfo)); //Scroll to Element Location
		 
		 String GuestName = FirstName.concat(" "+LastName);
		 
		 for(int i = 4; i<=13; i+=3) {
				
				logger.info(driver.findElement(By.xpath("//*[@id='book-content-left']/div[2]/div[3]/div/div[4]/div[" +String.valueOf(i) + "]/div/label")).getText() + ":" +driver.findElement(By.xpath("//*[@id='book-content-left']/div[2]/div[3]/div/div[4]/div[" +String.valueOf(i+1) + "]/div")).getText());
				
			 	//******************************Add Cookie************************************************//
				driver.manage().addCookie(new Cookie(driver.findElement(By.xpath("//*[@id='book-content-left']/div[2]/div[3]/div/div[4]/div[" +String.valueOf(i) + "]/div/label")).getText() ,driver.findElement(By.xpath("//*[@id='book-content-left']/div[2]/div[3]/div/div[4]/div[" +String.valueOf(i+1) + "]/div")).getText()));
				//******************************************************************************//
			}
		 
		 
		//GuestName
		Assert.assertEquals(GuestName, driver.manage().getCookieNamed("Guest Name").getValue());
			logger.info("GuestName Matched: "+ driver.manage().getCookieNamed("Guest Name").getValue());
			
		//Address
		Assert.assertEquals(Address, driver.manage().getCookieNamed("Address").getValue());
			logger.info("Address Matched: "+ driver.manage().getCookieNamed("Address").getValue());
			
		//PhoneNumber
		Assert.assertEquals(HomePhone, driver.manage().getCookieNamed("Phone Number").getValue());
			logger.info("Phone Number Matched: "+ driver.manage().getCookieNamed("Phone Number").getValue());
			
		//Email
		Assert.assertEquals(Email, driver.manage().getCookieNamed("E-mail Address").getValue());
			logger.info("E-mail Address Matched: "+ driver.manage().getCookieNamed("E-mail Address").getValue());
				
				
	}
	
	
}
