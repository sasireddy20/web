package membersArea_Reservations;

import java.text.ParseException;

import org.apache.log4j.Logger;
import org.openqa.selenium.Keys;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import login.Common;
import pageObjects.MembersArea;

public class FindVacation extends Common {
	
	 MembersArea MA = new MembersArea();//Page Object Element Locator
	 ReservationProcess ResProcess = new ReservationProcess();
	 Logger logger = Logger.getLogger("Book_with_FlexibleSearch.class");
	 
	 String Day; //Today
	 String ArrivalDate; //ArrivalDate
	 String Resort; //Name of Resort
	 String RoomType; //Room Type
	 String Points; //Points
	String ResortHeader = "Resort:"; //Header
	 
	 
	 @Test(priority=1)
	 @Parameters({"Destination","Nights"})
	 public void Flexible_Search_Calendar(String Destination, String Nights) throws InterruptedException, ParseException {
		 
			wait.until(ExpectedConditions.elementToBeClickable(MA.MembersArea_Reservations_Submit)); //Submit Button
			
			//Destination
			new Select(driver.findElement(MA.MembersArea_FindVacation_Destination)).selectByValue(Destination);
				logger.info("Selected Destination: "+Destination);
				
			//Nights
	  		driver.findElement(MA.MembersArea_FindVacation_Nights).sendKeys(Keys.BACK_SPACE);
	  			driver.findElement(MA.MembersArea_FindVacation_Nights).sendKeys(Nights);
	  				logger.info("Selected Nights");
			
	  				Thread.sleep(1000);
	  				
 			//Clicked on Search
			driver.findElement(MA.MembersArea_Reservations_Submit).click();
					logger.info("Clicked on Search");
					
			wait.until(ExpectedConditions.elementToBeClickable(MA.MembersArea_FindVacation_Results));
			
			driver.findElement(MA.MembersArea_FindVacation_Results).click();
				logger.info("Clicked on Results");
				
				
	 }

}
