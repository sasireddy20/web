package membersArea_Reservations;

import java.text.ParseException;
import java.util.Calendar;
import java.util.Date;
import org.apache.log4j.Logger;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import login.Common;
import pageObjects.MembersArea;

public class Book_with_SimpleSearch extends Common {
	
	 MembersArea MA = new MembersArea();
	 ReservationProcess ResProcess = new ReservationProcess();
	 Logger logger = Logger.getLogger("Book_with_SimpleSearch.class");
	 
	 String Day; //Today
	 String ArrivalDate; //ArrivalDate
	 
	 
	 @Test(priority=1)
	 @Parameters({"Destination","Nights"})
	 public void Simple_Search(String Destination, String Nights) throws InterruptedException, ParseException {
		 
			wait.until(ExpectedConditions.elementToBeClickable(MA.MembersArea_Reservations_VideoCloseButton)); //Submit Button
			
			driver.findElement(MA.MembersArea_Reservations_VideoCloseButton).click();
				logger.info("Clicked on Close");
			
			//*************************************For Elastic Search****************************************************//
				//Destination
				driver.findElement(MA.MembersArea_Reservations_Destination).sendKeys(Destination);
					Thread.sleep(1000);
					driver.findElement(MA.MembersArea_Reservations_Destination).sendKeys(Keys.ENTER);
						logger.info("Selected Destination: "+Destination);
				
			//*********************************************************************************************************//
					
				//ArrivalDate
				ArrivalDate = driver.findElement(MA.MembersArea_Reservations_ArrivalDate).getAttribute("value");
					logger.info("Arrival Date: "+ArrivalDate);
					
					Thread.sleep(1000);
				
				//Nights
		  		driver.findElement(MA.MembersArea_Reservations_Nights).sendKeys(Keys.BACK_SPACE);
		  			driver.findElement(MA.MembersArea_Reservations_Nights).sendKeys(Nights);
		  				logger.info("Selected Nights");
		  				
		  				Thread.sleep(1000);
					
				//Clicked on Search
				driver.findElement(MA.MembersArea_Reservations_Submit).click();
					logger.info("Clicked on Search");
					
					wait.until(ExpectedConditions.invisibilityOfElementLocated(MA.MembersArea_Loader)); //Element disappears
					
					
				if(driver.findElement(MA.NoSearchResults).isDisplayed()) {
					
					logger.info(driver.findElement(MA.NoSearchResults).getText());
					
					Search:
					for(int i=1; i<=100; i++)
					{
					
						Date date = dateFormat.parse(ArrivalDate);
							now.setTime(date); //Set the date
								now.add(Calendar.DATE, 10); //Add 3 days to the date
							
						ArrivalDate = dateFormat.format(now.getTime()); //Get the date after adding
							logger.info("Modified Date is: "+ArrivalDate);
							
							Thread.sleep(1000);
														
						//Change Arrival Date
						WebElement ArrDate = driver.findElement(MA.MembersArea_Reservations_ArrivalDate);
							ArrDate.sendKeys(Keys.chord(Keys.CONTROL, "a"));
								ArrDate.sendKeys(ArrivalDate);
									logger.info("Entered Arrival Date");
										driver.findElement(MA.MembersArea_Reservations_ArrivalDate2).sendKeys(Keys.TAB);
										Thread.sleep(2000);
										//Clicked on Search
										driver.findElement(MA.MembersArea_Reservations_Submit).click();
											logger.info("Clicked on Search");
											
											wait.until(ExpectedConditions.invisibilityOfElementLocated(MA.MembersArea_Loader)); //Element Disappears
											
											if(driver.findElement(MA.NoSearchResults).isDisplayed()) {
												
												logger.info(driver.findElement(MA.NoSearchResults).getText());
											}
											else {
												
												break Search;
											}
	
					
					}
					
				}
				else {
					
					if(driver.findElements(MA.SpecialOffersDialog).size()!=0) {
						jse.executeScript("window.scrollBy(0,-450)", "");
						Thread.sleep(1000);
						
						jse.executeScript("arguments[0].click();", driver.findElement(MA.SpecialOffersDialog));
							logger.info("Clicked No Thanks for Special offers");
							
								wait.until(ExpectedConditions.elementToBeClickable(MA.SearchResults_BookIt)); //First Search Result
					}
					else {
						
						wait.until(ExpectedConditions.elementToBeClickable(MA.SearchResults_BookIt)); //First Search Result
					}
					
				
				
				}
				
	 }
	 
	 @Test(priority=2)
	 @Parameters("Delinquency")
	 public void Simple_Search_ReservationProcess(String Delinquency) throws InterruptedException {
		 
		 
		 ResProcess.SearchResults();
		 ResProcess.Step2_ResortAndGuestDetails_ReviewReservation(Delinquency);
		 ResProcess.Step2_ResortAndGuestDetails_With_RequestsandAccessibility();
		 ResProcess.Step2_ResortAndGuestDetails_GuestInformation();
		 ResProcess.Step3_AdditionalOffers_TravelProtection();
		 ResProcess.Step3_AdditionalOffers_Miscellaneous_DeclinePurchase();
		 ResProcess.Step4_Payments_WithoutCCPayment();
		 ResProcess.FinalConfirmation();
		 
	 }
	 
	 @Test(priority=3)
	 @Parameters("Delinquency")
	 public void Simple_Search_ReservationProcess_RentPointsPaymentOptions(String Delinquency) throws InterruptedException {
		 
		 
		 ResProcess.SearchResults();
		 ResProcess.Step2_ResortAndGuestDetails_ReviewReservation(Delinquency);
		 ResProcess.Step2_ResortAndGuestDetails_Without_RequestsandAccessibility();
		 ResProcess.Step2_ResortAndGuestDetails_GuestInformation();
		 ResProcess.Step3_AdditionalOffers_TravelProtection();
		 ResProcess.Step3_AdditionalOffers_Miscellaneous_DeclinePurchase();
		 ResProcess.Step4_Payments_WithPaymentOptions();
		 ResProcess.FinalConfirmation();
		 
	 }
	 
	 @Test(priority=4)
	 @Parameters("Delinquency")
	 public void Simple_Search_ReservationProcess_WithMiscellaneousPurchase(String Delinquency) throws InterruptedException {
		 
		 
		 ResProcess.SearchResults();
		 ResProcess.Step2_ResortAndGuestDetails_ReviewReservation(Delinquency);
		 ResProcess.Step2_ResortAndGuestDetails_Without_RequestsandAccessibility();
		 ResProcess.Step2_ResortAndGuestDetails_GuestInformation();
		 ResProcess.Step3_AdditionalOffers_TravelProtection();
		 ResProcess.Step3_AdditionalOffers_Miscellaneous_Purchase();
		 ResProcess.Step4_Payments_MiscellaneousPurchase();
		 ResProcess.FinalConfirmation();
		 
	 }
	 
	 @Test(priority=5)
	 @Parameters("Delinquency")
	 public void Simple_Search_ReservationProcess_Delinquecy(String Delinquency) throws InterruptedException {
		 
		 
		 ResProcess.SearchResults();
		 ResProcess.Step2_ResortAndGuestDetails_ReviewReservation(Delinquency);
		 ResProcess.Step2_ResortAndGuestDetails_Without_RequestsandAccessibility();
		 ResProcess.Step2_ResortAndGuestDetails_GuestInformation();
		 ResProcess.Step3_AdditionalOffers_TravelProtection();
		 ResProcess.Step3_AdditionalOffers_Miscellaneous_DeclinePurchase();
		 ResProcess.Step4_Payments_Delinquency();
		 ResProcess.FinalConfirmation();
		 
	 }


}
