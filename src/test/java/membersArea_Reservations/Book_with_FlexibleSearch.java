package membersArea_Reservations;

import java.text.ParseException;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.Cookie;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.testng.Assert;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import login.Common;
import pageObjects.MembersArea;

public class Book_with_FlexibleSearch extends Common {
	
	 MembersArea MA = new MembersArea();//Page Object Element Locator
	 ReservationProcess ResProcess = new ReservationProcess();
	 Logger logger = Logger.getLogger("Book_with_FlexibleSearch.class");
	 
	 String Day; //Today
	 String ArrivalDate; //ArrivalDate
	 String Resort; //Name of Resort
	 String RoomType; //Room Type
	 String Points; //Points
	 String Checkin; //Checkin
	 String Checkout; //Checkout
	 String ResortHeader = "Resort:"; //Header
	 String ReservationConfirmationHeader = "Reservation Confirmation"; //Reservation Confirmation Header
	 String EstPointsAfterPurchase;
	 String PointsAfterPurchase;
	 
	 
	 @Test(priority=1)
	 @Parameters({"Destination","Nights"})
	 public void Flexible_Search_Calendar(String Destination, String Nights) throws InterruptedException, ParseException {
		 
			wait.until(ExpectedConditions.elementToBeClickable(MA.MembersArea_Reservations_Submit)); //Submit Button
			
			//Destination
			new Select(driver.findElement(MA.MembersArea_Reservations_Destination)).selectByValue(Destination);
				logger.info("Selected Destination: "+Destination);
				
			//Display results as
			new Select(driver.findElement(MA.MembersArea_Flexible_DisplayResultas)).selectByValue("Calendar");
				logger.info("Display Result as Calendar");
				
			//Nights
	  		driver.findElement(MA.MembersArea_Flexible_Nights).sendKeys(Keys.BACK_SPACE);
	  			driver.findElement(MA.MembersArea_Flexible_Nights).sendKeys(Nights);
	  				logger.info("Selected Nights");
	  				
	  				Thread.sleep(1000);
	  				
  			//Clicked on Search
			driver.findElement(MA.MembersArea_Reservations_Submit).click();
					logger.info("Clicked on Search");
					
					wait.until(ExpectedConditions.invisibilityOfElementLocated(MA.MembersArea_Loader)); //Element disappears

				if(driver.findElement(MA.NoSearchResults).getText().substring(0,34).equals("There is currently no availability")) { //No Results Displayed
					
					logger.info(driver.findElement(MA.NoSearchResults).getText());
					
					//Select Month
					Search:
						for(int i=1; i<=12; i++)
						{
							driver.findElement(By.xpath("//*[@id='MonthId']/option["+String.valueOf(i)+"]")).click();	
								logger.info("Selected Month");
								
								//Clicked on Search
								driver.findElement(MA.MembersArea_Reservations_Submit).click();
									logger.info("Clicked on Search");
								
								if(driver.findElement(MA.MembersArea_Flexible_NextMnth).isDisplayed()) {
									logger.info("Calendar Displayed");
									break Search;
									
								}
								
								else {
									
									logger.info(driver.findElement(MA.NoSearchResults).getText());
								}
						
						}
				}
				
				else {
					
					logger.info("Search Results are Available");
				}
				
				Search:
				for(int i  = 1; i<=100; i++)
				{
					Resort = driver.findElement(By.xpath("//*[@id='booking-calendar-"+Destination+"']/div[2]/div["+String.valueOf(i)+"]/div/a")).getText();
						logger.info("Resort Name: "+Resort);
					
					RoomType = driver.findElement(By.xpath("//*[@id='booking-calendar-"+Destination+"']/div[3]/div/div/div/div/div["+String.valueOf(i)+"]/div/span")).getText();
						logger.info("Room Type: "+RoomType);
						
					Points = driver.findElement(By.xpath("//*[@id='booking-calendar-"+Destination+"']/div[3]/div/div/div/div/div["+String.valueOf(i)+"]/div/b")).getText();
						logger.info("Point Cost: "+Points);
						
					WebElement ResultClick = driver.findElement(By.xpath("//*[@id='booking-calendar-"+Destination+"']/div[3]/div/div/div/div/div["+String.valueOf(i)+"]/div/span"));
						actions.moveToElement(ResultClick).click().perform();		
						
						wait.until(ExpectedConditions.invisibilityOfElementLocated(MA.MembersArea_Loader)); //Element Disappears
					 	Thread.sleep(1000);
					 	
					 	//Resort not available
					 	if(driver.findElements(By.xpath("//button[text()='Ok']")).size()!=0) //Resort not available 
						{
					 		if(driver.findElement(By.xpath("//*[@id='alert-dlg']")).getText().substring(0,34).equals("In accordance with Club Guidelines")) {
					 			
					 			Points = String.valueOf(Integer.parseInt(Points)/2);
					 				logger.info(driver.findElement(By.xpath("//*[@id='alert-dlg']")).getText());
					 					driver.findElement(By.xpath("//button[text()='Ok']")).sendKeys(Keys.ENTER);
					 						logger.info("Clicked on Ok");
					 							break Search;
					 		}
					 		else {
					 		driver.findElement(By.xpath("//button[text()='Ok']")).sendKeys(Keys.ENTER);
					 			logger.info("Unfortunely selected resort is unavabilable - Clicked on OK button");
					 		}
					 		
						}
						
						else
						{
							break Search;
						}
						
				}
				
	 }
				
	 @Test(priority = 2)
	 @Parameters("Delinquency")
	 public void Step2_ResortAndGuestDetails_ReviewReservation(String Delinquency) throws InterruptedException {
	 
		wait.until(ExpectedConditions.textToBe(MA.ReservationSummary_ResortHeader, ResortHeader)); //Text Is Present
		
			//-------------------------------------------Review Your Reservation----------------------------------------//
		
		 //Reservation Summary
		 
		 //Resort
		 Assert.assertEquals(driver.findElement(MA.ReservationSummary_Resort).getText(), Resort);
		 	logger.info("Resort Name Matched: "+driver.findElement(MA.ReservationSummary_Resort).getText());
		 	
		 //Room Type
	 	 Assert.assertEquals(driver.findElement(MA.ReservationSummary_RoomType).getText().replaceAll(" ", "").trim(), RoomType.replaceAll(" ", "").trim());
		 	logger.info("Room Type Name Matched: "+driver.findElement(MA.ReservationSummary_RoomType).getText());	
		 	
	 	//Points
	 	 Assert.assertEquals(driver.findElement(MA.ReservationSummary_Points).getText(), Points);
		 	logger.info("Points Matched: "+driver.findElement(MA.ReservationSummary_Points).getText());	
		 	
		 //Total Payments
	 	if(Delinquency.equals("Y")) {
	 		Assert.assertEquals(driver.findElement(MA.TotalPayments_Delinquency).getText().replaceAll("USD", "").trim(), driver.manage().getCookieNamed("TotalPayments").getValue());
	 			logger.info("Total Payments Matched: "+driver.findElement(MA.TotalPayments_Delinquency).getText());
	 	}
	 	
	 	else {
	 	Assert.assertEquals(driver.findElement(MA.TotalPayments).getText().replaceAll("USD", "").trim(), driver.manage().getCookieNamed("TotalPayments").getValue());
	 		logger.info("Total Payments Matched: "+driver.findElement(MA.TotalPayments).getText());
	 	}
		 	
		 	jse.executeScript("arguments[0].scrollIntoView(true);",  driver.findElement(MA.Next_Button)); //Scroll to Element Location
		 	
		 	//Checkin Date
		 	Checkin =  driver.findElement(MA.ReservationSummary_Checkin).getText();
			 	logger.info("Checkin Date: "+Checkin);	
				 	
		 	//Checkout
		 	Checkout =  driver.findElement(MA.ReservationSummary_Checkout).getText();
			 	logger.info("Checkout Date Matched: "+Checkout);
			 	
		 //Next Button
		 driver.findElement(MA.Next_Button).click();
		 	logger.info("CLicked on Next Button - Step2 Review your Reservation");
		 	
		 	
		 	//-------------------------------------Methods from Reservation process.java------------------------------------//
			 ResProcess.Step2_ResortAndGuestDetails_Without_RequestsandAccessibility();
			 ResProcess.Step2_ResortAndGuestDetails_GuestInformation();
			 ResProcess.Step3_AdditionalOffers_TravelProtection();
			 ResProcess.Step3_AdditionalOffers_Miscellaneous_DeclinePurchase();
			 ResProcess.Step4_Payments_WithoutCCPayment();
			//-------------------------------------------------------------------------------------------------------------//
			 
			 wait.until(ExpectedConditions.elementToBeClickable(MA.FinalConfirm_SendEmail)); //Send Email
				
				//Reservation Confirmation
				Assert.assertEquals(driver.findElement(MA.FinalConfirm_ReservationConfirmation_Header).getText(), ReservationConfirmationHeader);
			 		logger.info("Reservation Confirmation header Matched: "+driver.findElement(MA.FinalConfirm_ReservationConfirmation_Header).getText());
			 		
			 		//Estimate Points After Purchase
					EstPointsAfterPurchase = String.valueOf(Integer.parseInt(driver.manage().getCookieNamed("PointsBeforePurchase").getValue()) - Integer.parseInt(Points));
						logger.info("Points After Purchase Should be: "+EstPointsAfterPurchase);
						
						//Points After Purchase
						PointsAfterPurchase = driver.findElement(MA.MembesArea_AvailablePoints).getText();
							Assert.assertEquals(PointsAfterPurchase, EstPointsAfterPurchase);
								logger.info("Points Matched: "+PointsAfterPurchase);
								
	 				for(int i = 2; i<=20; i+=3) {
	 				
						logger.info(driver.findElement(By.xpath("//*[@id='book-content-left']/div[2]/div[3]/div/div[3]/div[" +String.valueOf(i) + "]/div/label")).getText() + ":" +driver.findElement(By.xpath("//*[@id='book-content-left']/div[2]/div[3]/div/div[3]/div[" +String.valueOf(i+1) + "]/div")).getText());
	 					
					 	//******************************Add Cookie************************************************//
						driver.manage().addCookie(new Cookie(driver.findElement(By.xpath("//*[@id='book-content-left']/div[2]/div[3]/div/div[3]/div[" +String.valueOf(i) + "]/div/label")).getText(),driver.findElement(By.xpath("//*[@id='book-content-left']/div[2]/div[3]/div/div[3]/div[" +String.valueOf(i+1) + "]/div")).getText()));
						//******************************************************************************//
					}
	 			

	 				//CheckIn Date Compare
	 				Assert.assertEquals(Checkin.toUpperCase(), driver.manage().getCookieNamed("Check-In Date:").getValue().toUpperCase());
	 					logger.info("Check In Date Matched: "+ driver.manage().getCookieNamed("Check-In Date:").getValue().toUpperCase());
	 					
					//Checkout Date Compare
	 				Assert.assertEquals(Checkout.toUpperCase(), driver.manage().getCookieNamed("Check-Out Date:").getValue().toUpperCase());
	 					logger.info("Check Out Date Matched: "+ driver.manage().getCookieNamed("Check-Out Date:").getValue().toUpperCase());
	 					
	 				//Total Points Used
	 				Assert.assertEquals(Points, driver.manage().getCookieNamed("Total Points Used:").getValue());
	 					logger.info("Total Points Used Matched: "+ driver.manage().getCookieNamed("Total Points Used:").getValue());
			 	
	 }
	 
 
 }

