package membersArea_Reservations;

import java.util.List;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.testng.Assert;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import login.Common;
import pageObjects.MembersArea;

public class CancelReservation extends Common {
	
	MembersArea MA = new MembersArea(); 
	
	Logger logger = Logger.getLogger("CancelReservation.class");
	String ConfirmedReservations = "Confirmed Reservations";
	String cellval; //Value
	String SuccessMessgae = "Your cancellation request has been processed successfully";
	
	@Test
	@Parameters("CancelReservation")
	public void CancelConfirmedReservation(String CancelReservation) throws InterruptedException {
		
			//Members Area Bread Crumb
			driver.findElement(MA.MembersArea_BreadCrumb).click();
				logger.info("Clicked on Member Area Bread Crumb");
				
				wait.until(ExpectedConditions.elementToBeClickable(MA.MembersArea_MakePayment)); //wait till Members Area Bread Crumb is clickable
				logger.info("Element is Clickable");
				
				//-----------------------------Click on initialized Element---------------------------------------------------------//
				
				List<WebElement> elements = driver.findElements(By.tagName("a")); // Lists all the elements

		        for (WebElement element : elements) {
		        	
		        	String Name = element.getText(); //Get the Names from tag a
		        		logger.info(""+Name);
		        	
		            if(Name.equals(CancelReservation)) {
		            	
		            	logger.info("Name Matched: "+CancelReservation);
		            	
		            	jse.executeScript("arguments[0].click();", element); //Click on the name
		            		logger.info("Clicked on: "+CancelReservation);
		            			break;
		            }
		        }
				
			
		    //---------------------------------------------------------------------------------------------------------------------//
		        
		        wait.until(ExpectedConditions.textToBe(MA.ConfirmReservation_Header, ConfirmedReservations.toUpperCase())); //Text Is Present
		        
		        WebElement dateWidget = driver.findElement(By.xpath("//*[@id='open-reservation-table']/tbody"));
				List<WebElement> columns=dateWidget.findElements(By.tagName("tr"));
						int rowsnum1 = columns.size();
							String xpath =null;
								
									Search:
										for(int i = 1;i<=rowsnum1;i++)
										{
											xpath = "//*[@id='open-reservation-table']/tbody/tr[" + String.valueOf(i) + "]/td[1]";
											cellval = driver.findElement(By.xpath(xpath)).getText();
											
											if(driver.manage().getCookieNamed("Confirmation Number:").getValue().equals(cellval)) {

												WebElement ConfNum = driver.findElement(By.xpath("//*[@id='open-reservation-table']/tbody/tr[" + String.valueOf(i) + "]/td[7]/a[2]"));
													actions.moveToElement(ConfNum).click().perform();
														logger.info("Clicked on Cancel");
															break Search;
												
											}
											
											else {
												
												logger.info("Confirmation Number Didnt Match");
											}
										}
							
					wait.until(ExpectedConditions.elementToBeClickable(MA.CancelReservation)); //wait till Cancel Reservation Button is Clickable
					Thread.sleep(1000);
					jse.executeScript("arguments[0].scrollIntoView(true);",  driver.findElement(MA.CancelReservation)); //Scroll to Element Location
					Thread.sleep(1000);
					
					driver.findElement(MA.CancelReservation).click();
						logger.info("Clicked on Cancel Reservation Button");
						
						wait.until(ExpectedConditions.elementToBeClickable(MA.SuccessMessage_BookNowLink)); //Element disappears

						Assert.assertEquals(driver.findElement(MA.SuccessMessage).getText().substring(0, 57), SuccessMessgae);
							logger.info("Reservation Successfully Cancelled:");

		
	}

}
