package pageObjects;

import org.openqa.selenium.By;

public class MembersArea_Redesign {

	//Cookie Banner
	public By CookieBanner_Ok = By.xpath("//*[@id='cookielawBanner']/div[2]/a[1]"); //Cookie Banner Ok Button
	
	//Return to Classic Site - Close and Return to Classic Site
	public By MA_Redesign_ReturnToClassicSite = By.xpath("//*[@id='btnFeedbackFormClose']"); //Close and Return To Classic Site
	
	//Landing page
	public By MA_Redesign_MemberHomePage = By.xpath("//*[@id='welcomeMsg']/div[2]/a"); //New Member Home Page
	
	//Points
	public By MA_Redesign_Points = By.xpath("//*[@class='box-contents-line-seperation']/div[2]"); //Points
	
	// Search
	public By MA_Redesign_Search_Goingto = By.xpath("//*[@id='DestinationSearchTerm']"); //Going to
	public By MA_Redesign_Search_Nights = By.xpath("//*[@id='Booking_NightCount']"); //Nights
	public By MA_Redesign_Search_Search = By.xpath("//*[@id='submit']"); //Search button
	
		
		//Search Results
		public By MA_Redesign_Search_SearchResults_OpenMap = By.xpath("//*[@id='map-button-container']/a"); //Open Map
		public By MA_Redesign_Search_PropertyResults = By.xpath("//*[@id='results-body']/div[1]/div[1]"); //Property results
		public By MA_Redesign_Search_ChangeView = By.xpath("//*[@id='dropdown-changeview']"); //Change View
		public By MA_Redesign_Search_SearchRequest = By.xpath("//*[@id='search-request-link']"); //Search Request
		
		
		//Guest Info - Review 
		public By MA_Redesign_Review_BreadCrumb = By.xpath("//div[@class='breadcrumbWrap']/ul[1]/li[3]"); //Review BreadCrumb
		public By MA_Redesign_CompleteYourReservation_Header = By.xpath("//div[@class='box-top-bigger']"); //Complete your reservation header
		public By MA_Redesign_Reservation_CancelReservation_Button = By.xpath("//a[@id='CancelButton']"); //Cancel Reservation
	
		public By MA_Redesign_Reservation_Review_Next_Button = By.linkText("Next"); //Next 
		public By MA_Redesign_Reservation_Next_Button = By.xpath("//*[@value='Next']"); //Next 
		
		//GuestInfo - Accomodations - From MemberArea Reused Xpaths
		
		//GuestInfo - Guest Info
		public By MA_Redesign_Reservation_EditInfo = By.xpath("//*[@id='AjaxEditInfo']"); //Edit Information
		public By MA_Redesign_Reservation_GuestInfo_Header = By.xpath("//*[@id='PrimaryOverlapPresentText']/b"); //HEader
		public By MA_Redesign_Reservation_GuestInfo_GuestMessage = By.xpath("//*[@id='PrimaryOverlapPresentText']/span[1]"); //HEader
		
		//Special Offers
		//Vacation Guard Travel Club Plan
		public By MA_Redesign_VacationGuard_Header = By.xpath("//*[@id='travelInsHeading']"); // VACATIONGUARD® TRAVEL CLUB PLAN Header
	
		
		//Legal Protection plan - A Travel Benefit
		public By MA_Redesign_LegalProtectionPlan_Header = By.xpath("//*[@id='legalPlan']/div[1]"); // LEGAL PROTECTION PLAN - A TRAVEL BENEFIT Header

		
		//Skymed
		public By MA_Redesign_Skymed_Header = By.xpath("//*[@id='skyMed']/div[1]"); // SKYMED Header
		
		// Healthiest You - A Travel Benefit
		public By MA_Redesign_HealthiestYou_Header = By.xpath("//*[@id='md247Plan']/div[1]"); // Healthiest You - A Travel Benefit
		
		//Total Payments
		public By MA_Redesign_Total = By.xpath("//div[@class='total']/div[2]"); //TOtal**
		
		//Confirmation Number
		public By MA_Redesign_ConfirmationNumber = By.xpath("//div[@class='user-form2']/div[2]/div"); //Confirmation Number
		
		
		
}
