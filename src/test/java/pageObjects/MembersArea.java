package pageObjects;

import org.openqa.selenium.By;


public class MembersArea {
	
	//Cookie Banner Please Wait
	public By CookieBanner_Ok = By.xpath("//*[@id='lnkOkButton']"); //Cookie Banner Ok Button
	public By MembersArea_Loader = By.xpath("//*[@id='ajaxLoader']/div[2]"); //Loader
	
	//Login
	public By MembersArea_Username = By.xpath(".//*[@id='UserName']"); //Username
	public By MembersArea_Password = By.xpath(".//*[@id='Password']"); //Password
	public By MembersArea_Submit = By.xpath("//input[@type='submit']"); //Submit 
	public By MembersArea_Submit1 = By.xpath("//*[@class='form-group']/button"); //Submit 
	public By MembersArea_MultipleAccounts = By.xpath("//*[@class='pretty zebra tbl-responsive']/tbody/tr[1]/td[1]/a"); //Submit
	
	//Landing page
	public By MembersArea_BreadCrumb = By.xpath(".//*[@id='breadcrumb']/li[1]/h3/a"); //Breads Crumb
	public By MembesArea_AvailablePoints = By.xpath("//*[@id='dashboard-content']/div[4]/div/strong[2]"); //Points Before Purchase
	
	//---------------------------------------------------------------------------------------My ACcount-------------------------------------------------------------------------------------------------------------------//
	
	//Make payments - Membership Receivables
	public By MembersArea_MembershipReceivables_PastDue = By.xpath("//*[@id='mem-rec-table']/tbody/tr[2]/td[3]/div[2]"); //Make Payments
	
	public By MembersArea_MakePayment = By.xpath("//*[@id='dashboard-content']/div[3]/div/div[2]/a"); //Make Payments
	public By MembersArea_MakePayment_SaveButton = By.xpath("//*[@id='submitButton']"); //Save Button - (Near Billing Statement)
	
	
	//---------------------------------------------------------------------------------------My Reservations-------------------------------------------------------------------------------------------------------------------//
	
	//Simple Search and Flexible Search
	public By MembersArea_Reservations_VideoCloseButton = By.xpath("/html/body/div[10]/div[1]/button");//Video Tutorial close button;
	public By MembersArea_Reservations_Submit = By.xpath("//*[@id='submit']"); //Simple Search Submit
	public By MembersArea_Reservations_Destination = By.xpath("//*[@id='DestinationSearchTerm']"); //Destination
	public By MembersArea_Reservations_ArrivalDate = By.xpath("//*[@id='Booking_CheckInFromDate']"); //Arrival date Between
	public By MembersArea_Reservations_ArrivalDate2 = By.xpath("//*[@id='Booking_CheckInToDate']"); //Arrival date Between
	public By MembersArea_Reservations_Nights = By.xpath("//*[@id='Booking_NightCount']"); //Nights
	
	
	//Flexible Search
	public By MembersArea_Flexible_DisplayResultas = By.xpath("//*[@id='DisplayModeId']"); //Display Result as
	public By MembersArea_Flexible_Month = By.xpath("//*[@id='MonthId']"); //Months
	public By MembersArea_Flexible_Nights = By.xpath("//*[@id='Nights2']"); //Nights
	public By MembersArea_Flexible_NextMnth = By.xpath("//*[@id='NextMonth']"); //Next Month
	
	//Find Vacation
	public By MembersArea_FindVacation_Destination = By.xpath("//*[@id='DestinationDDL']"); //Destination Find Vacation
	public By MembersArea_FindVacation_Nights = By.xpath("//*[@id='Booking_NightCount']"); //Nights
	public By MembersArea_FindVacation_Complete = By.xpath("//*[@id='search-results']/div[1]/div[1]"); //Complete Bar
	public By MembersArea_FindVacation_Results = By.xpath("//*[@id='search-results']/div[3]/div[1]"); //Complete Bar

	//Search Results
	public By SpecialOffersDialog = By.xpath("//a[@class='soItemNotInterested']"); //Special Offers Dialog Box
	public By NoSearchResults = By.xpath("//*[@id='AvailResults']/div"); //NO Search Results
	public By SearchResults_Location = By.xpath("//*[@id='GridResultsTable']/tbody/tr[1]/td[1]/p");// Search Results_Location
	public By SearchResults_Resort = By.xpath("//*[@id='GridResultsTable']/tbody/tr[1]/td[2]/p/a");// Search Results_Resort
	public By SearchResults_RoomType = By.xpath("//*[@id='GridResultsTable']/tbody/tr[1]/td[3]/p/a");// Search Results_Room Type
	public By SearchResults_Checkin = By.xpath("//*[@id='GridResultsTable']/tbody/tr[1]/td[4]/p");// Search Results_Checkin
	public By SearchResults_Checkout = By.xpath("//*[@id='GridResultsTable']/tbody/tr[1]/td[5]/p");// Search Results_Checkout
	public By SearchResults_Points = By.xpath("//*[@id='GridResultsTable']/tbody/tr[1]/td[6]/p");// Search Results_Points
	public By SearchResults_BookIt = By.xpath("//*[@id='GridResultsTable']/tbody/tr[1]/td[7]/div/p/a");// Search Results_Book It
	
	//********************************************************************************************************************************************************************************************************************//
	
	//Step2 Resort and Guest Details (Review your Reservation, Requests and Accessibility, Guest Information)
	
	//-------------------------------------Review your Reservation-----------------------------------------------------------------//
	//Reservation Summary
	public By ReservationSummary_ResortHeader = By.xpath("//*[@id='content-right']/div/div[4]/div[1]/div[1]"); //Resort Header
	public By ReservationSummary_Resort = By.xpath("//*[@id='content-right']/div/div[4]/div[1]/div[2]");// ReservationSummary_Resort
	public By ReservationSummary_RoomType = By.xpath("//*[@id='content-right']/div/div[4]/div[2]/div[2]");// ReservationSummary_Room Type
	public By ReservationSummary_Checkin = By.xpath("//*[@id='content-right']/div/div[4]/div[3]/div[2]");// ReservationSummary_Checkin
	public By ReservationSummary_Checkout = By.xpath("//*[@id='content-right']/div/div[4]/div[4]/div[2]");// ReservationSummary_Checkout
	public By ReservationSummary_Points = By.xpath("//*[@id='content-right']/div/div[4]/div[5]/div[2]");// ReservationSummary_Points
	
	//TotalPayments
	public By TotalPayments = By.xpath("//*[@id='content-right']/div/div[6]/div[1]/div[2]");// Total payments Amount
	public By TotalPayments_Delinquency = By.xpath("//*[@id='content-right']/div/div[6]/div[5]/div[2]");// Total payments Delinquency Amount
	
	//Click on Next
	public By Next_Button = By.xpath("//*[@id='submitButton']"); //Next Button
	
	//-------------------------------------Requests and Accessibility-------------------------------------------------------------//
	public By Requests = By.xpath("//*[@id='SpecialRequests']"); //Enter Requests 
	public By HearingImpairment = By.xpath("//*[@id='SpecialNeedsModel_HasHearingImpairment']"); //Hearing Impairment Check Box
	public By Partial_Mobility_Impairment = By.xpath("//*[@id='SpecialNeedsModel_HasPartialMobilityImpairment']"); //Partial Mobility Impairment
	public By Visual_Impairment = By.xpath("//*[@id='SpecialNeedsModel_HasVisualImpairment']"); //Visual Impairment
	public By WheelChair = By.xpath("//*[@id='SpecialNeedsModel_IsWheelchairUser']"); //WheelChair 
	public By MedicalCondition = By.xpath("//*[@id='SpecialNeedsModel_HasPreExistingMedicalCondition']"); //Pre-Existing Medical Condition 
	public By ServiceDog = By.xpath("//*[@id='SpecialNeedsModel_IsServiceAnimalUser']"); //Service Dog
	public By Disability_Assistance = By.xpath("//*[@id='SpecialRequirements']"); //Disability or require accessibility Assistance
	
	//Click on Next
	public By ReqAcc_Next = By.xpath("//*[@id='book-content-left']/div[2]/form/div[4]/input"); //Next Button in Requests and Accessibility
	
	//--------------------------------------Guest Information----------------------------------------------------------------------------//
	
	//Reservation Summary
	public By SpecialAccommodations = By.xpath("//*[@id='content-right']/div/div[4]/div[8]"); //Special Accommodations
	
	//Guest Info
	public By GuestInfo_Member_Myself = By.xpath("//*[@id='Member_Myself']"); //Member(Myself)
	public By GuestInfo_Guest = By.xpath("//*[@id='Guest']"); //Guest
	public By GuestInfo_NewGuest = By.xpath("//*[@id='NewGuest']"); //New Guest
	public By GuestInfo_SavedGuests = By.xpath("//*[@id='SavedGuest']"); //Saved Guests
	public By GuestInfo_GuestCharge = By.xpath("//*[@id='GuestFeeLabel']"); //Guest Charge for New or Saved Guests
	public By GuestInfo_FirstName = By.xpath("//*[@id='BookingMemberInfo_FirstName']"); //FirstNAme
	public By GuestInfo_LastName = By.xpath("//*[@id='BookingMemberInfo_LastName']"); //Last Name
	public By GuestInfo_Address = By.xpath("//*[@id='BookingMemberInfo_Address1']"); //Address
	public By GuestInfo_Country = By.xpath("//*[@id='BookingMemberInfo_Country']"); //Country
	public By GuestInfo_PostalCode = By.xpath("//*[@id='BookingMemberInfo_PostalCode']"); //Postal Code
	public By GuestInfo_CityTown = By.xpath("//*[@id='BookingMemberInfo_City']"); //City/Town
	public By GuestInfo_State = By.xpath("//*[@id='BookingMemberInfo_State']"); //State
	public By GuestInfo_Email = By.xpath("//*[@id='BookingMemberInfo_Email']"); //Email
	public By GuestInfo_VerifyEmail = By.xpath("//*[@id='BookingMemberInfo_VerifyEmail']"); //VerifyEmail
	public By GuestInfo_HomePhone = By.xpath("//*[@id='BookingMemberInfo_HomePhone']"); //Homephone
	public By GuestInfo_WorkPhone = By.xpath("//*[@id='BookingMemberInfo_WorkPhone']"); //WorkPhone
	
	//Click on Next
	public By GuestInfo_Next = By.xpath("//*[@id='nextButton']"); //Next Button
	
	//********************************************************************************************************************************************************************************************************************//
	
	//Step3 - Additional Offers (Travel Protection, Miscellaneous)
	
	//------------------------------------------Travel Protection---------------------------------------------------------------------//
	public By TermsConditons = By.xpath("//*[@id='UnderstandTerms']"); //Terms and Conditions
	public By RPP_Message= By.xpath("//*[@id='book-content-left']/div[2]/form/div[1]/div[1]/div/strong"); //RPP Purchase MEssage.
	public By TravelProtection_Next = By.xpath("//*[@id='book-content-left']/div[2]/form/div[3]/input"); //Travel Protection Next Button
	
	//------------------------------------------Miscellaneous---------------------------------------------------------------------//
	
	//Vacation Guard Travel Club Plan
	public By VacationGuard_Header = By.xpath("//*[@id='travelInsHeading']/h1"); // VACATIONGUARD® TRAVEL CLUB PLAN Header
	public By VacationGuard_Authorize = By.xpath("//*[@id='TravelInsurance_true']"); // VACATIONGUARD® TRAVEL CLUB PLAN (Authorize purchase)
	public By VacationGuard_Decline = By.xpath("//*[@id='TravelInsurance_false']"); // VACATIONGUARD® TRAVEL CLUB PLAN (Decline purchase)
	
	//Legal Protection plan - A Travel Benefit
	public By LegalProtectionPlan_Header = By.xpath("//*[@id='lppHeading']/h1"); // LEGAL PROTECTION PLAN - A TRAVEL BENEFIT Header
	public By LegalProtectionPlan_Decline = By.xpath("//*[@id='LegalProtectionPlan_No']"); // LEGAL PROTECTION PLAN - A TRAVEL BENEFIT (Decline purchase)
	
	//Skymed
	public By Skymed_Header = By.xpath("//*[@id='skyMedHeading']/h1"); // SKYMED Header
	public By Skymed_FamilyUSD = By.xpath("//*[@id='skyMedFamily']"); // SKYMED Family USD
	public By Skymed_Decline = By.xpath("//*[@id='skyMed_No']"); // SKYMED (Decline purchase)
	
	//DialCare - A Travel Benefit
	public By DialCare_Header = By.xpath("//*[@id='md247Heading']/h1"); //DIALCARE - A TRAVEL BENEFIT Header
	public By DialCare_Decline = By.xpath("//*[@id='MD247_No']"); //DIALCARE - A TRAVEL BENEFIT (Decline Purchase)
	
	//CLick on Next
	public By Miscellaneous_Next = By.xpath("//*[@id='book-content-left']/div[2]/form/div[7]/input"); //Next
		
	//********************************************************************************************************************************************************************************************************************//
	//Step4 - Payments
	
	//Total Payments
	public By Step4_TotalPayments = By.xpath("//*[@id='content-right']/div/div[6]/div[1]/div[2]"); //Total Payemnts
			
	public By Step4_TotalPayments_RentPointsCost = By.xpath("//*[@id='content-right']/div/div[6]/div[1]/div[1]"); //Rent Points Cost
	public By Step4_TotalPayments_RentPointsCostUSD = By.xpath("//*[@id='content-right']/div/div[6]/div[1]/div[2]"); //Rent Points Cost USD
	
	//Payment Options
	public By PaymentOptions = By.xpath("//*[@id='book-content-left']/div[2]/div[2]/a"); //Payment Options
	public By PaymentOptions_NumofPoints = By.xpath("//*[@id='ReservationCostPoints']"); //Number of Points
	public By PaymentOptions_AvailPoints = By.xpath("//*[@id='PaymentOptionsForm']/div/div[2]/div[3]/div[3]/div/div[2]/div[2]"); //Available Points
	public By PaymentOptions_Allotment = By.xpath("//*[@id='UseTermPointOptions_ALT__PointsToUse']"); //Allotment
	public By PaymentOptions_RentPoints = By.xpath("//*[@id='RentalPoints']"); //Rent Points
	public By PaymentOptions_RentPoints_Remainder = By.xpath("//*[@id='Remainder']"); //Remainder
	public By PaymentOptions_Refresh = By.xpath("//*[@id='refresh']"); //Refresh
	public By PaymentOptions_Next = By.xpath("//*[@id='PaymentOptionsForm']/div/div[3]/input[3]"); //Next 
	public By PayemntOptions_Total = By.xpath("//*[@id='TotalCost']"); //Total Cost to rent points
	
	//Credit Card Information
	public By AddCard = By.xpath("//*[@id='AddCardLink']"); //Add card Link
	public By AddButton = By.xpath("//*[@id='btnAddCardSubmit']"); //Add Button
	public By CardNumber = By.xpath("//*[@id='CreditCardItem_CardNumber']"); //Card Number
	public By CardHolderName = By.xpath("//*[@id='CreditCardItem_CardHolder']"); //Card Holder Name
	public By ExpMonth = By.xpath("//*[@id='CreditCardItem_Month']"); //Expiration Date (Month)
	public By ExpYear = By.xpath("//*[@id='CreditCardItem_Year']"); //Expiration Date (Year)
	public By CVV = By.xpath("//*[@id='CVV2']"); //CVV
	public By SelectCard = By.xpath("//*[@id='SelectedCreditCardId_0']"); //Select Credit Card
	
	
	//Creditcard Approval Terms Check box
	public By Creditcard_Terms = By.xpath("//*[@id='SelectedShowAndGo']"); //Creditcard Approval Terms Check box
	
	//Authorize payment check box
	public By AuthorizePayment = By.xpath("//*[@id='Authorize']"); //Authorize payment check box
	
	//No reservation fee 
	public By NoResFees= By.xpath("//*[@id='book-content-left']/div[2]/form"); //No Res Fees
	
	//Confirm Reservation
	public By Confirm_Reservation = By.xpath("//*[@id='credit-card-list']"); //Confirm Reservation Button
	
	//********************************************************************************************************************************************************************************************************************//
	
	//Final Confirmation
	public By FinalConfirm_ReservationConfirmation_Header = By.xpath("//*[@id='book-content-left']/div[2]/div[2]/h2"); //Reservation Confirmation Header
	public By FinalConfirm_SendEmail = By.xpath("//*[@id='Email']"); //Email Text Box
	public By FinalConfirm_SendEmail_Button = By.xpath("//*[@id='submitButton']"); //send email button
	public By FinalConfirm_SendEmail_SuccessMes = By.xpath("//*[@id='successMessage']"); //Success Message
	public By FinalConfirm_ChangeGuestInfo = By.xpath("//*[@id='book-content-left']/div[2]/div[3]/div/div[4]/div[2]/div/a");
	
	//Confirmed Reservations
	public By ConfirmReservation_Header = By.xpath("//*[@id='content-body']/div/h1"); // Header
	public By CancelReservation = By.xpath("//*[@id='formSubmit']"); // Cancel Reservation
	public By SuccessMessage = By.xpath("//*[@id='successMessage']"); //Success Message
	public By SuccessMessage_BookNowLink = By.xpath("//*[@id='successMessage']/a"); //Success Message

	
	
	
	

	
}
