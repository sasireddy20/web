package login;

import java.io.IOException;
import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;
import pageObjects.*;


public class Login extends Common {

	static Logger logger = Logger.getLogger("Login.class");
	MembersArea MA = new MembersArea();

	@Parameters({"browserType", "appURL" })
	@BeforeClass
	public void initializeTestBaseSetup(String browserType, String appURL) throws IOException {
	
		try {
			
			System.setProperty(FirefoxDriver.SystemProperty.BROWSER_LOGFILE,"DeleteThisLog"); //Stops printing the browser log files in console and creates a txt file called "DeleteThisLog"		 
				setDriver(browserType, appURL);
			

		} catch (Exception e) {
			logger.info("Error....." + e.getStackTrace());
		}
			
	}
	
	private void setDriver(String browserType, String appURL) {
		switch (browserType) {
		
		case "chrome":
			driver = initChromeDriver(appURL);
			break;
			
		case "firefox":
			driver = initFirefoxDriver(appURL);
			break;
			
		default:
			System.out.println("browser : " + browserType + " is invalid, Launching Firefox as browser of choice..");
			driver = initFirefoxDriver(appURL);
		}
	}
	
	private static WebDriver initChromeDriver(String appURL) {
		logger.info("Launching Chrome browser..");
		 ChromeOptions chromeOptions = new ChromeOptions();
		 	chromeOptions.addArguments("--start-maximized");
		 		driver = new ChromeDriver(chromeOptions);
		 			wait = new WebDriverWait(driver, 130);
		 				longWait = new WebDriverWait(driver, 100);
		 				actions = new Actions(driver);
		 				jse = (JavascriptExecutor)driver;
		 					driver.get(appURL);
		 						return driver;
	}


	
	private static WebDriver initFirefoxDriver(String appURL) {
		logger.info("Launching Firefox browser..");
			driver = new FirefoxDriver();
				driver.manage().window().maximize();
					wait = new WebDriverWait(driver, 130);
						actions = new Actions(driver);
							jse = (JavascriptExecutor)driver;
								longWait = new WebDriverWait(driver, 100);
										driver.get(appURL);
											return driver;
	}
	
	
	@Test
	@Parameters({"Username","Password"})
	public void MembersAreaLogin(String Username, String Password) {
		
		 PropertyConfigurator.configure("Log4j.properties");
		
		//Username
		driver.findElement(MA.MembersArea_Username).sendKeys(Username);
			logger.info("Entered Username: "+Username);
			
		//Password
		driver.findElement(MA.MembersArea_Password).sendKeys(Password);
			logger.info("Entered Password: "+Password);
			
	
		//Submit	 
		if(driver.findElements(MA.MembersArea_Submit).size()>0) {
		
			driver.findElement(MA.MembersArea_Submit).click();
				logger.info("Clicked on submit");
		}
		
		else {
			driver.findElement(MA.MembersArea_Submit1).click();
			logger.info("Clicked on submit");
			
		}
		
		//Multiple Accounts
		if(driver.findElements(MA.MembersArea_MultipleAccounts).size()!=0) {
			
			jse.executeScript("arguments[0].click();", driver.findElement(MA.MembersArea_MultipleAccounts));
			logger.info("Clicked on First Account");
		}
		
		else {
			
			logger.info("No Multiple Accounts");
		}
	}
}
