package login;

import java.util.Iterator;
import java.util.Set;

import org.testng.annotations.Test;

public class CloseBrowser extends Common {
	
	@Test
	public void Close_AllBrowsers() {
		
		Set<String> window = driver.getWindowHandles(); // Get all the window handles
		
		Iterator<String> Iterate= window.iterator(); // Iterate through
		
		while(Iterate.hasNext())
		{	 
		   String window_handels = Iterate.next(); // Goes through all the windows
           	driver.switchTo().window(window_handels); // Switch through the window
           		driver.close(); // Close Browser     		
		}
			
	}
	
}
