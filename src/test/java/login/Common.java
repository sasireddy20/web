package login;

import java.io.FileInputStream;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import org.apache.log4j.PropertyConfigurator;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.FluentWait;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.BeforeSuite;



public class Common {
  
  public static WebDriver driver; 
  public static WebDriverWait wait;
  public static WebDriverWait longWait;
  public static FluentWait<WebDriver> fluentwait;
  public DateFormat dateFormat = new SimpleDateFormat("dd-MMM-yyyy"); //Date Format
  public Calendar now = Calendar.getInstance(); // Calendar instance
  public Date date = new Date(); //Date 
  protected static Actions actions; 
  public static JavascriptExecutor jse; 

  public static String UsernameDB; //UsernameDB
  public static String PasswordDB; //PasswordDB
  public static String UsernameOutlook; //Outlook Username
  public static String PasswordOutlook; //Outlook password
  
  //Guest Information
  public String FirstName = "Diamond";
  public String LastName = "Tester";
  public String Address = "10600 W Charleston Blvd";
  public String Country = "USA";
  public String PostalCode = "89135";
  public String CityTown = "Las Vegas";
  public String State = "NV";
  public static String Email = "Sasi.Reddy@diamondresorts.com";
  public String HomePhone = "3033033333";
  
  //Credit Card
  public String CCNumber = "4321000000001119";
  public String CCName = "Diamond Tester";
  public String ExpMonth = "01";
  public String ExpYr = "2023";
  public String Cvv = "333";
  
  
 
  
  @BeforeSuite
  public void ReadExcel() throws IOException {
    
    PropertyConfigurator.configure("Log4j.properties");
    
    //Load file
    FileInputStream fileInputStream = new FileInputStream(UserData.FilePath);
    
    //Load WorkBook
    XSSFWorkbook workbook = new XSSFWorkbook(fileInputStream);
    
    //Load Sheet
    XSSFSheet worksheet = workbook.getSheet(UserData.Worksheet_Name);
    
    XSSFCell Clarity_UsernameDB = worksheet.getRow(4).getCell(0);
      UsernameDB  =  Clarity_UsernameDB.getStringCellValue();
      //UserData.logger.info("ClarityDB Username: "+UsernameDB);
    
    XSSFCell Clarity_PasswordDB = worksheet.getRow(4).getCell(1);
      PasswordDB =  Clarity_PasswordDB.getStringCellValue();
      //UserData.logger.info("DB-Password Retrieved: "+PasswordDB);
    
    XSSFCell Outlook_Username = worksheet.getRow(7).getCell(0);
      UsernameOutlook  =  Outlook_Username.getStringCellValue();
      //UserData.logger.info("Outlook Username: "+UsernameOutlook);
    
    XSSFCell Outlook_PasswordDB = worksheet.getRow(7).getCell(1);
      PasswordOutlook =  Outlook_PasswordDB.getStringCellValue();
      //UserData.logger.info("Outlook Password Retrieved: "+PasswordOutlook);
    
    workbook.close();
  }
  
  public boolean WaitforJSandJQueryToLoad() {
  	
		// wait for jQuery to load
	    ExpectedCondition<Boolean> jQueryLoad = new ExpectedCondition<Boolean>() {
	      @Override
	      public Boolean apply(WebDriver driver) {
	        try {
	          return (Long) jse.executeScript("return jQuery.active") == 0;
	        }
	        catch (Exception e) {
	          // no jQuery present
	          return true;
	        }
	      }
	    };
	    
	
	    // wait for Javascript to load
	    ExpectedCondition<Boolean> jsLoad = new ExpectedCondition<Boolean>() {
	      @Override
	      public Boolean apply(WebDriver driver) {
	        return (jse).executeScript("return document.readyState")
	        .toString().equals("complete");
	      }
	    };
	    
	
	  return wait.until(jQueryLoad) && wait.until(jsLoad);
  
	}
  
 
  
    
}