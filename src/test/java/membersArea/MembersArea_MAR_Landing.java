package membersArea;

import java.util.List;
import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;
import login.Common;
import pageObjects.MembersArea;
import pageObjects.MembersArea_Redesign;

public class MembersArea_MAR_Landing extends Common{
	
	 Logger logger = Logger.getLogger("MembersArea_Redesign_Landing.class");
	 MembersArea MA = new MembersArea();
	 MembersArea_Redesign MA_Redesign = new MembersArea_Redesign();
	 
	 String PointsBeforePurchase; //Points  Before Purchase
	 String TotalPayments = "$0.00"; //Total payments in reservation step 2 - comparison
	 public static String AvailablePoints = ""; //Points
	
	@Test
	@Parameters({"InitalizedName","InitalizedName1"})
	public void LandingPage(String InitalizedName, String InitalizedName1) throws InterruptedException {
	
	logger.info("Logged into Members Area");
	
	wait.until(ExpectedConditions.elementToBeClickable(MA_Redesign.MA_Redesign_MemberHomePage)); //wait till New Members Area Home Page Button
		logger.info("Element is Clickable");
		
		driver.findElement(MA_Redesign.MA_Redesign_MemberHomePage).click();
			logger.info("Clicked on New Members Area Home PAge Button");
		
		//-------------------------------------------------------------------------Points-------------------------------------------------------------------------------//
			AvailablePoints = driver.findElement(MA_Redesign.MA_Redesign_Points).getText().replaceAll(",", "");
				logger.info("Available Points: "+AvailablePoints);
				
			
			//Cookie Banner
			driver.findElement(MA_Redesign.CookieBanner_Ok).click();
				logger.info("Clicked on Ok");
			
			wait.until(ExpectedConditions.invisibilityOfElementLocated(MA.MembersArea_Loader)); //Loading......
			
		
				
			//-----------------------------Click on initialized Element---------------------------------------------------------//
					
					List<WebElement> elements = driver.findElements(By.tagName("a")); // Lists all the elements

			        for (WebElement element : elements) {
			        	
			        	String Name = element.getText(); //Get the Names from tag a
			        		logger.info(""+Name);
			        		
			        	
			            if(Name.equals(InitalizedName)) {
			            	
			            	logger.info("Name Matched");
			            	
			            	jse.executeScript("arguments[0].click();", element); //Click on the name
			            		logger.info("Clicked on: "+InitalizedName);
			            			
			            			
			            			
			            }
			            
			            if(Name.equals(InitalizedName1)) {
            				
            				jse.executeScript("arguments[0].click();", element); //Click on the name
		            			logger.info("Clicked on:" +InitalizedName1);
		            			
		            			break;
            				
            				
            			}
			        }
					
					
	}

}
