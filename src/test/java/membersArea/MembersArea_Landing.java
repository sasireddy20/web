package membersArea;

import java.util.List;
import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.Cookie;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import login.Common;
import pageObjects.MembersArea;
import pageObjects.MembersArea_Redesign;

public class MembersArea_Landing extends Common{
	
	 Logger logger = Logger.getLogger("MembersArea_Landing.class");
	 MembersArea MA = new MembersArea();
	 MembersArea_Redesign MA_Redesign = new MembersArea_Redesign();
	 
	 String PointsBeforePurchase; //Points  Before Purchase
	 String TotalPayments = "$0.00"; //Total payments in reservation step 2 - comparision
	
	@Test
	@Parameters({"InitalizedName"})
	public void LandingPage(String InitalizedName) throws InterruptedException {
	
	logger.info("Logged into Members Area");
	
	wait.until(ExpectedConditions.elementToBeClickable(MA.MembersArea_BreadCrumb)); //wait till Members Area Bread Crumb is clickable
		logger.info("Element is Clickable");
		
		//-------------------------------------------------------------------------Points-------------------------------------------------------------------------------//
		
			/*driver.findElement(MA.CookieBanner_Ok).click();
				logger.info("Clicked on Ok");*/
			
		//	wait.until(ExpectedConditions.invisibilityOfElementLocated(MA.CookieBanner_Loading)); //Loading......
		
		//New Members Area
		if(driver.findElements(MA_Redesign.MA_Redesign_MemberHomePage).size()!=0) {
			
			driver.findElement(MA_Redesign.MA_Redesign_MemberHomePage).click();
				logger.info("Clicked on New Members Area Home PAge Button");
				
				
			driver.findElement(By.xpath(".//a[@data-ajax-begin='FeedbackButtonBegin_switch']")).click();
				logger.info("Clicked on return to classic site");
				
				wait.until(ExpectedConditions.elementToBeClickable(MA_Redesign.MA_Redesign_ReturnToClassicSite)); //close and Return To Classic Site
				
				driver.findElement(MA_Redesign.MA_Redesign_ReturnToClassicSite).click();
					logger.info("Clicked on Close & Return To Clssic Site");
					
					Thread.sleep(2000);
					
					wait.until(ExpectedConditions.elementToBeClickable(driver.findElement(By.xpath("//*[@class='redirect-popup-btn']")))); //Ok  Button
					
					driver.findElement(By.xpath("//*[@class='redirect-popup-btn']")).click();
						logger.info("Clicked on OK");
				
		}
		
			
			wait.until(ExpectedConditions.elementToBeClickable(MA.MembersArea_BreadCrumb)); //wait till Members Area Bread Crumb is clickable
			
			//Points Available Before Purchase
			PointsBeforePurchase = driver.findElement(MA.MembesArea_AvailablePoints).getText();
				logger.info("Points Before Purchase is: "+PointsBeforePurchase);
				
			
			//******************************Add Cookie************************************************//
			driver.manage().addCookie(new Cookie("PointsBeforePurchase",PointsBeforePurchase));
			driver.manage().addCookie(new Cookie("TotalPayments",TotalPayments));
			//******************************************************************************//
				
			//-----------------------------Click on initialized Element---------------------------------------------------------//
					
					List<WebElement> elements = driver.findElements(By.tagName("a")); // Lists all the elements

			        for (WebElement element : elements) {
			        	
			        	String Name = element.getText(); //Get the Names from tag a
			        		logger.info(""+Name);
			        	
			            if(Name.equals(InitalizedName)) {
			            	
			            	logger.info("Name Matched");
			            	
			            	jse.executeScript("arguments[0].click();", element); //Click on the name
			            		logger.info("Clicked on: "+InitalizedName);
			            			break;
			            }
			        }
					
					
	}

}
