package membersArea_MAR_Reservations;

import org.apache.log4j.Logger;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.testng.Assert;
import org.testng.annotations.Test;

import login.Common;
import pageObjects.MembersArea;
import pageObjects.MembersArea_Redesign;

public class MAR_Reservation_SpecialOffers extends Common {
	
	Logger logger = Logger.getLogger("MAR_Reservation_SpecialOffers.class");
	MembersArea MA = new MembersArea();
	MembersArea_Redesign MA_Redesign = new MembersArea_Redesign();
	
	String jsExecute = "arguments[0].scrollIntoView(true);"; 
	String VacationGuard = "VacationGuard® Travel Club Plan *"; //VacationGuard® Travel Club Plan * header
 	String LegalProtectionPlan = "Legal Protection Plan - A Travel Benefit"; //Legal Protection Plan - a Travel Benefit header
 	String SkyMed = "SkyMed"; //SkyMed header
 	String HealthiestYou = "Healthiest You - A Travel Benefit"; //Healthiest You - A Travel Benefit
 	String Miscellaneous = null;//

	@Test(priority=1)
	public void SpecialOffers_TravelProtection() throws InterruptedException {
		
		//Wait
		wait.until(ExpectedConditions.elementToBeClickable(MA_Redesign.MA_Redesign_Reservation_Next_Button)); //Wait until Next button is clickable 
		Thread.sleep(1000);
		
		//BreadCrumb - Review
		Assert.assertEquals("Travel Protection", driver.findElement(MA_Redesign.MA_Redesign_Review_BreadCrumb).getText());
			logger.info("Bread Crumb MAtched: "+driver.findElement(MA_Redesign.MA_Redesign_Review_BreadCrumb).getText());
		
		
		//Click on Terms 
		driver.findElement(MA.TermsConditons).click();
		logger.info("Clicked on Terms and Conditions");
		
		jse.executeScript(jsExecute,  driver.findElement(MA_Redesign.MA_Redesign_Reservation_Next_Button)); //Scroll to Element Location
		
		//Total
		if(MAR_Reservation_GuestInfo.NewGuest.equals("Y")) {
			
			Assert.assertEquals(driver.findElement(MA_Redesign.MA_Redesign_Total).getText().replaceAll("[^0-9^.]",""), MAR_Reservation_GuestInfo.GuestCharge);
			logger.info("TOtal Payment Assertion Passed: "+driver.findElement(MA_Redesign.MA_Redesign_Total).getText().replaceAll("[^0-9^.]",""));
			
		}
		
		else {
		Assert.assertEquals(driver.findElement(MA_Redesign.MA_Redesign_Total).getText().replaceAll("[^0-9^.]",""), MAR_Reservation_GuestInfo.Total);
		logger.info("TOtal Payment Assertion Passed");
		}
		
		
		//Next Button
		driver.findElement(MA_Redesign.MA_Redesign_Reservation_Next_Button).click();
			logger.info("Clicked on Next");

			
	}
	
	@Test(priority=2)
	public void SpecialOffers_Miscellaneous() throws InterruptedException {
		
		wait.until(ExpectedConditions.textToBe(MA_Redesign.MA_Redesign_LegalProtectionPlan_Header, LegalProtectionPlan)); //Text Is Present
		Thread.sleep(2000);
		
 		Miscellaneous = "N"; //for step4 process
		
		//Vacation Guard Travel Club Plan
		 Assert.assertEquals(driver.findElement(MA_Redesign.MA_Redesign_VacationGuard_Header).getText().toUpperCase(), VacationGuard.toUpperCase());
		 	logger.info("VACATIONGUARD® TRAVEL CLUB PLAN * header Matched: "+driver.findElement(MA_Redesign.MA_Redesign_VacationGuard_Header).getText());
		 	
		 	//Decline Vacation Guard
		 	driver.findElement(MA.VacationGuard_Decline).click();
		 		logger.info("Declined Vacation Guard");
		 		

		//Legal Protection Plan
		 Assert.assertEquals(driver.findElement(MA_Redesign.MA_Redesign_LegalProtectionPlan_Header).getText().toUpperCase(), LegalProtectionPlan.toUpperCase());
		 	logger.info("LEGAL PROTECTION PLAN - A TRAVEL BENEFIT header Matched: "+driver.findElement(MA_Redesign.MA_Redesign_LegalProtectionPlan_Header).getText());
		 	
		 	//Decline Legal Protection Plan
		 	driver.findElement(MA.LegalProtectionPlan_Decline).click();
		 		logger.info("Declined Legal Protection Plan");
		 		
		 		Thread.sleep(1000);
		 		
		 		jse.executeScript(jsExecute,  driver.findElement(MA.Skymed_Decline)); //Scroll to Element Location
		 		
		//Skymed
		 Assert.assertEquals(driver.findElement(MA_Redesign.MA_Redesign_Skymed_Header).getText().toUpperCase(), SkyMed.toUpperCase());
		 	logger.info("SKYMED header Matched: "+driver.findElement(MA_Redesign.MA_Redesign_Skymed_Header).getText());
		 	
		 	//Decline Skymed
		 	driver.findElement(MA.Skymed_Decline).click();
		 		logger.info("Declined SkyMed");	
		 		
		 		Thread.sleep(1000);
		 		
		 		jse.executeScript(jsExecute,  driver.findElement(MA.DialCare_Decline)); //Scroll to Element Location
		
		// Healthiest You - A Travel Benefit
		Assert.assertEquals(driver.findElement(MA_Redesign.MA_Redesign_HealthiestYou_Header).getText().toUpperCase(), HealthiestYou.toUpperCase());
	 		logger.info("HEALTHIEST YOU - A TRAVEL BENEFIT header Matched: "+driver.findElement(MA_Redesign.MA_Redesign_HealthiestYou_Header).getText());
			 	
		 	//Decline  Healthiest You - A Travel Benefit
		 	driver.findElement(MA.DialCare_Decline).click();
		 		logger.info("Declined  Healthiest You - A Travel Benefit");	
		 		
				 		
		 //click on Next
		 driver.findElement(MA_Redesign.MA_Redesign_Reservation_Next_Button).click();
		 	logger.info("Clicked on Next");		
	 		
	}


}
