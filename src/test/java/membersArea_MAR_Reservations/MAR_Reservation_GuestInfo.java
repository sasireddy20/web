package membersArea_MAR_Reservations;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.testng.Assert;
import org.testng.annotations.Test;

import login.Common;
import membersArea_Reservations.ReservationProcess;
import pageObjects.MembersArea;
import pageObjects.MembersArea_Redesign;

public class MAR_Reservation_GuestInfo extends Common {
	
	Logger logger = Logger.getLogger("MAR_Reservation_GuestInfo.class");
	MembersArea MA = new MembersArea();
	MembersArea_Redesign MA_Redesign = new MembersArea_Redesign();
	ReservationProcess Members_ReservationProcess = new ReservationProcess(); 
	
	String jsExecute = "arguments[0].scrollIntoView(true);"; 
	String CompleteYourReservation;
	  //Account Details
	  public String MyPoints; //My Points
	  public String ArrivalDate; //Arrival Date
	  public String DepartureDate; //Departure Date
	  public String RoomType; //Room Type
	  public String Nights; //Nights
	  public String Adults; //Adults
	  public String Children; //Children
	  public String PointsRequired; //Points Required
	  public static String Total = "";//Total
	  public static String GuestCharge = "";//Guest Charge
	  public static String NewGuest = "";
	
	
	@Test(priority=1)
	public void GuestInfo_Review() throws InterruptedException {
		
		//Wait
		wait.until(ExpectedConditions.elementToBeClickable(MA_Redesign.MA_Redesign_Reservation_Review_Next_Button)); //Wait until Next button is clickable 
		Thread.sleep(1000);
		
		//BreadCrumb - Review
		Assert.assertEquals("Review", driver.findElement(MA_Redesign.MA_Redesign_Review_BreadCrumb).getText());
			logger.info("Bread Crumb MAtched: "+driver.findElement(MA_Redesign.MA_Redesign_Review_BreadCrumb).getText());
		
		//Complete your Reservation Header
		CompleteYourReservation = driver.findElement(MA_Redesign.MA_Redesign_CompleteYourReservation_Header).getText();
				Assert.assertEquals("Complete Your Reservation", CompleteYourReservation);
					logger.info("Header Matched: "+CompleteYourReservation);
		
		//Arrival Date
		ArrivalDate = driver.findElement(By.xpath("//div[@class='box-contents']/div[1]/div[1]/div[1]/div[@class='content-right']/label")).getText();
			logger.info("ArrivalDate: "+ArrivalDate);
			
		//Departure Date
		DepartureDate = driver.findElement(By.xpath("//div[@class='box-contents']/div[1]/div[1]/div[2]/div[@class='content-right']/label")).getText();
			logger.info("DepartureDate: "+DepartureDate);
			
		//RoomType Date
		RoomType = driver.findElement(By.xpath("//div[@class='box-contents']/div[1]/div[1]/div[3]/div[@class='content-right']/label")).getText();
			logger.info("RoomType: "+RoomType);
			
		//Nights
		Nights = driver.findElement(By.xpath("//div[@class='box-contents']/div[1]/div[1]/div[4]/div[@class='content-right']/label")).getText();
			logger.info("Nights: "+Nights);	
			
		//Adults
		Adults = driver.findElement(By.xpath("//div[@class='box-contents']/div[1]/div[1]/div[5]/div[@class='content-right']/label")).getText();
			logger.info("Adults: "+Adults);	
			
		//Children
		Children = driver.findElement(By.xpath("//div[@class='box-contents']/div[1]/div[1]/div[6]/div[@class='content-right']/label")).getText();
			logger.info("Children: "+Children);	
			
		//PointsRequired
		PointsRequired = driver.findElement(By.xpath("//div[@class='box-contents']/div[1]/div[1]/div[7]/div[@class='content-right']/label")).getText();
			logger.info("PointsRequired: "+PointsRequired);
			
		//Total Payments
		Total = driver.findElement(By.xpath("//div[@class='total']/div[2]")).getText().replaceAll("[^0-9^.]","");
			logger.info("Total Payments: "+Total);
			
			
		jse.executeScript(jsExecute,  driver.findElement(MA_Redesign.MA_Redesign_Reservation_Review_Next_Button)); //Scroll to Element Location
		
		//Next Button
		driver.findElement(MA_Redesign.MA_Redesign_Reservation_Review_Next_Button).click();
			logger.info("Clicked on Next");

			
	}
	
	@Test(priority=2)
	public void GuestInfo_Accomodations() throws InterruptedException {
		
		//Wait
		wait.until(ExpectedConditions.elementToBeClickable(MA.Requests)); //Special Request
		Thread.sleep(1000);
		
		//BreadCrumb - Review
		Assert.assertEquals("Accomodations", driver.findElement(MA_Redesign.MA_Redesign_Review_BreadCrumb).getText());
			logger.info("Bread Crumb MAtched: "+driver.findElement(MA_Redesign.MA_Redesign_Review_BreadCrumb).getText());
				
		
		//Special Request
		driver.findElement(MA.Requests).sendKeys("Diamond Resorts International");
			logger.info("Entered Special Requests");
			
		//Accessibility
			
			//Hearing Impairment
			driver.findElement(MA.HearingImpairment).click();
				logger.info("Clicked on Hearing Impairment");
				
			//Partial Mobility Impairment
			driver.findElement(MA.Partial_Mobility_Impairment).click();
				logger.info("Clicked on Partial Mobility Impairment");
				
			jse.executeScript(jsExecute,  driver.findElement(MA_Redesign.MA_Redesign_Reservation_Next_Button)); //Scroll to Element Location
			
			//Visual Impairment
			driver.findElement(MA.Visual_Impairment).click();
				logger.info("Clicked on Visual Impairment");
				
			//Wheelchair user
			driver.findElement(MA.WheelChair).click();
				logger.info("Clicked on Wheelchair user");
				
			//Pre-Existing Medical Condition
			driver.findElement(MA.MedicalCondition).click();
				logger.info("Clicked on Pre-Existing Medical Condition");
				
			//Uses Service Dog
			driver.findElement(MA.ServiceDog).click();
				logger.info("Clicked on Uses Service Dog");
			
		//Next Button
		driver.findElement(MA_Redesign.MA_Redesign_Reservation_Next_Button).click();
			logger.info("Clicked on Next");
	}
	
	@Test(priority=3)
	public void GuestInfo_Guest_Info() throws InterruptedException {
		
		wait.until(ExpectedConditions.elementToBeClickable(MA_Redesign.MA_Redesign_Reservation_Next_Button)); //Next Button
		Thread.sleep(1000);
		
		//BreadCrumb - Guest Info
		Assert.assertEquals("Guest Info", driver.findElement(MA_Redesign.MA_Redesign_Review_BreadCrumb).getText());
			logger.info("Bread Crumb MAtched: "+driver.findElement(MA_Redesign.MA_Redesign_Review_BreadCrumb).getText());
			
			
			
			if(driver.findElement(MA.GuestInfo_Member_Myself).isEnabled()) {

				NewGuest = "N"; //For Total Comparision
					
				jse.executeScript(jsExecute,  driver.findElement(MA_Redesign.MA_Redesign_Reservation_Next_Button)); //Scroll to Element Location
					
				//Next Button
				driver.findElement(MA_Redesign.MA_Redesign_Reservation_Next_Button).click();
					logger.info("Clicked on Next");
				
				
			}
			
			else {
				
				NewGuest = "Y"; //For Total Comparision
				
				logger.info(driver.findElement(MA_Redesign.MA_Redesign_Reservation_GuestInfo_GuestMessage).getText());
				
				GuestCharge = driver.findElement(MA.GuestInfo_GuestCharge).getText().replaceAll("[^0-9^.]","");
					logger.info(GuestCharge);
				
				Members_ReservationProcess.NewGuest();
				
				
			
			}
	}


}
