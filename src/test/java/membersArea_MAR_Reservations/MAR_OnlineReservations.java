package membersArea_MAR_Reservations;


import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import login.Common;
import pageObjects.MembersArea;
import pageObjects.MembersArea_Redesign;

public class MAR_OnlineReservations extends Common {
	
	Logger logger = Logger.getLogger("BasicSearch.class");
	 MembersArea MA = new MembersArea();
	MembersArea_Redesign MA_Redesign = new MembersArea_Redesign();
	String jsExecute = "arguments[0].scrollIntoView(true);"; 
	String Property;
	String PropertyId;
	public static String TotalPoints_SearchResults = "";//Total

	
	@Test(priority=1)
	@Parameters({"Goingto","Nights"})
	public void BasicSearchReservation(String Goingto, String Nights) throws InterruptedException {
		
		//Wait
		wait.until(ExpectedConditions.elementToBeClickable(MA_Redesign.MA_Redesign_Search_Search)); //Wait until Search button is clickable 
		
		//GoingTo
		driver.findElement(MA_Redesign.MA_Redesign_Search_Goingto).sendKeys(Goingto);
			logger.info("Entered Goingto: "+Goingto);
			Thread.sleep(1000);
			
		//Nights
		driver.findElement(MA_Redesign.MA_Redesign_Search_Nights).sendKeys(Keys.chord(Keys.CONTROL,"a"));
			driver.findElement(MA_Redesign.MA_Redesign_Search_Nights).sendKeys(Nights);
				logger.info("Entered No. of Nights");
				
		//Search
		driver.findElement(MA_Redesign.MA_Redesign_Search_Search).click();
			logger.info("Clicked on Search");
			
	}
	
	
	@Test(priority=2)
	public void BasicSearch_Grid() throws InterruptedException {
		
			wait.until(ExpectedConditions.invisibilityOfElementLocated(MA.MembersArea_Loader)); //Please Wait
			
			wait.until(ExpectedConditions.elementToBeClickable(MA_Redesign.MA_Redesign_Search_SearchResults_OpenMap)); //Wait until OpenMap is clickable 
		
				//If Property
				if(driver.findElements(MA_Redesign.MA_Redesign_Search_PropertyResults).size() !=0) { 
					
					//Get Id of Property
					Property = driver.findElement(MA_Redesign.MA_Redesign_Search_PropertyResults).getAttribute("id");
						PropertyId = 	Property.substring(Property.length()-3);
							logger.info("First Property Id is: "+PropertyId);
							
							
							if(driver.findElements(By.id("rs-detail-collapse-DRI-"+PropertyId+"")).size()!=0) {
								
								//View Choices	
								driver.findElement(By.id("rs-detail-collapse-DRI-"+PropertyId+"")).click();	
									logger.info("Clicked on View Choices");
									
							}
							
							wait.until(ExpectedConditions.elementToBeClickable(By.id("rowDetails-DRI-"+PropertyId+""))); //Wait until OpenMap is clickable 
								Thread.sleep(1000);
							
								Search:
								for(int i = 1; i<=20; i++) {
							
									//Book It Button
									driver.findElement(By.xpath("//*[@id='rowDetails-DRI-"+PropertyId+"']/div["+String.valueOf(i)+"]//a[@class='blue-button']")).click();	
										logger.info("Clicked on Book It Button");
											Thread.sleep(2000);
											
											
											//Resort not available
										 	if(driver.findElements(By.xpath("//div[@class='modal-buttons']/a[@class='blue-button']")).size()!=0) //Resort not available 
											{
								 					driver.findElement(By.xpath("//div[@class='modal-buttons']/a[@class='blue-button']")).click();
								 						logger.info("Clicked on Ok");
										 								 
										 							
									 		}
										 	
										 	else {
										 		
										 		break Search;
										 	}

											
								}
								
								
					}
	

		}
	
	@Test(priority=3)
	public void BasicSearch_Calendar() throws InterruptedException {
		
			wait.until(ExpectedConditions.invisibilityOfElementLocated(MA.MembersArea_Loader)); //Please Wait
			
			wait.until(ExpectedConditions.elementToBeClickable(MA_Redesign.MA_Redesign_Search_SearchResults_OpenMap)); //Wait until OpenMap is clickable 
			
			new Select(driver.findElement(MA_Redesign.MA_Redesign_Search_ChangeView)).selectByValue("Calendar");
				logger.info("Selected Calendar as Change View");
		
				wait.until(ExpectedConditions.elementToBeClickable((By.id("NextMonth")))); //Wait until OpenMap is clickable
					Thread.sleep(1000);
				
				Search:
					for(int i = 1; i<=20; i++) {
						
						jse.executeScript("window.scrollBy(0, 300)");
						
						TotalPoints_SearchResults= 	driver.findElement(By.xpath("//*[@class='fc-event-container']/div["+String.valueOf(i)+"]/div/b")).getText();	
							logger.info("Point Cost: "+TotalPoints_SearchResults);
							
							driver.findElement(By.xpath("//*[@class='fc-event-container']/div["+String.valueOf(i)+"]/div/b")).click();
								logger.info("Clicked on the Result");
								
								Thread.sleep(2000);
								
								//Resort not available
							 	if(driver.findElements(By.xpath("//div[@class='modal-buttons']/a[@class='blue-button']")).size()!=0) //Resort not available 
								{
					 					driver.findElement(By.xpath("//div[@class='modal-buttons']/a[@class='blue-button']")).click();
					 						logger.info("Clicked on Ok");
							 								 
							 							
						 		}
							 	
							 	else {
							 		
							 		break Search;
							 	}

					}
	}
	

}
