package membersArea_MAR_Reservations;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.testng.Assert;
import org.testng.annotations.Test;

import login.Common;
import membersArea_Reservations.ReservationProcess;
import pageObjects.MembersArea;
import pageObjects.MembersArea_Redesign;

public class MAR_Reservation_Payment extends Common {
	
	Logger logger = Logger.getLogger("MAR_Reservation_SpecialOffers.class");
	MembersArea MA = new MembersArea();
	MembersArea_Redesign MA_Redesign = new MembersArea_Redesign();
	ReservationProcess MAResProc = new ReservationProcess();

	
	String jsExecute = "arguments[0].scrollIntoView(true);"; 
	

	@Test(priority=1)
	public void Reservation_Payments() throws InterruptedException {
		
		//Wait
		wait.until(ExpectedConditions.elementToBeClickable(MA.Confirm_Reservation)); //Wait until Next button is clickable 
		Thread.sleep(1000);
		
		MAResProc.AddCreditCard(); //Add Credit Card
		
		Thread.sleep(1000);
		
		jse.executeScript(jsExecute,  driver.findElement(MA_Redesign.MA_Redesign_Total)); //Scroll to Element Location
		
		//Total
		if(MAR_Reservation_GuestInfo.NewGuest.equals("Y")) {
			
			Assert.assertEquals(driver.findElement(MA_Redesign.MA_Redesign_Total).getText().replaceAll("[^0-9^.]",""), MAR_Reservation_GuestInfo.GuestCharge);
				logger.info("TOtal Payment Assertion Passed: "+driver.findElement(MA_Redesign.MA_Redesign_Total).getText().replaceAll("[^0-9^.]",""));
			
		}
		
		else {
			
			Assert.assertEquals(driver.findElement(MA_Redesign.MA_Redesign_Total).getText().replaceAll("[^0-9^.]",""), MAR_Reservation_GuestInfo.Total);
				logger.info("TOtal Payment Assertion Passed");
		
		}
		
		jse.executeScript(jsExecute,  driver.findElement(MA.Confirm_Reservation)); //Scroll to Element Location
		
		//Confirm Reservation Button
		driver.findElement(MA.Confirm_Reservation).click();
			logger.info("Clicked on Confirm Reservations");
			
			try {
			if(driver.findElement(By.xpath("//*[@id='bookingPaymentValidationSummary']/div/ul/li")).getText().equals("Authorization Is Required")) {
				
				//Enter CVV
				driver.findElement(MA.CVV).sendKeys(Cvv);
					logger.info("Entered Cvv");
					
				//Authorize Payment
				driver.findElement(MA.AuthorizePayment).click();
					logger.info("Clicked on Authorize Payment");
					
					//Confirm Reservation Button
					driver.findElement(MA.Confirm_Reservation).click();
						logger.info("Clicked on Confirm Reservations");
				}
			}
			
			catch(Exception e) {

			}
	
			
	}
	
	
}
