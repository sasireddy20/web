package membersArea_MAR_Reservations;

import org.apache.log4j.Logger;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.testng.annotations.Test;

import login.Common;
import membersArea_Reservations.ReservationProcess;
import pageObjects.MembersArea;
import pageObjects.MembersArea_Redesign;

public class MAR_Reservation_FinalConfirmation extends Common {
	
	Logger logger = Logger.getLogger("MAR_Reservation_SpecialOffers.class");
	MembersArea MA = new MembersArea();
	MembersArea_Redesign MA_Redesign = new MembersArea_Redesign();
	ReservationProcess MAResProc = new ReservationProcess();
	String EmailMessage;
	public static String ConfirmationNumber = ""; //Confirmation Number

	
	String jsExecute = "arguments[0].scrollIntoView(true);"; 
	

	@Test(priority=1)
	public void Confirmation() throws InterruptedException {
		
		wait.until(ExpectedConditions.elementToBeClickable(MA.FinalConfirm_SendEmail)); //Send Email
		

	 	//Send Email
	 	driver.findElement(MA.FinalConfirm_SendEmail).sendKeys(Email);
	 		logger.info("Entered Email Address");
	 	
	 	//Send Email Button
	 	driver.findElement(MA.FinalConfirm_SendEmail_Button).click();
	 		logger.info("Clicked on Send Email Button");
	 		
 		wait.until(ExpectedConditions.visibilityOfElementLocated(MA.FinalConfirm_SendEmail_SuccessMes)); //Text Is Present
 		
 		EmailMessage = driver.findElement(MA.FinalConfirm_SendEmail_SuccessMes).getText();
 			logger.info(EmailMessage);
 		
 		//Confirmation Number
		ConfirmationNumber = driver.findElement(MA_Redesign.MA_Redesign_ConfirmationNumber).getText();
			logger.info("Confirmation Number: "+ConfirmationNumber);
 			

			
	}
	
	
}
