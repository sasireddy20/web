package com.report;

import java.util.Properties;

import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.activation.FileDataSource;
import javax.mail.BodyPart;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;

import login.Common;


public class SendMail  {

  static String Testoutput_Filepath = System.getProperty("user.dir")+"\\";

public static void execute(String reportFileName, String SuiteName) throws InterruptedException {
  
    // Create object of Property file
    Properties props = new Properties();
    
    //Authentication to TRUE
    props.put("mail.smtp.auth", "true");
    props.put("mail.smtp.starttls.enable", "true");
    
    //Host Server
    props.put("mail.smtp.host", "smtp-mail.outlook.com");
    //Port of SMTP Server
    props.put("mail.smtp.port", "587");
    
    //Authentication Handle
    Session session = Session.getInstance(props,
      new javax.mail.Authenticator() {
        protected PasswordAuthentication getPasswordAuthentication() {
            return new PasswordAuthentication(Common.UsernameOutlook,Common.PasswordOutlook);
        }
      });
    
    try {
      
      //Create MimeMessage Class
        Message message = new MimeMessage(session);
        
        //Set from Email Address
        message.setFrom(new InternetAddress(Common.UsernameOutlook)); //Enter Username
        
        //Recipient Address
       // message.setRecipients(Message.RecipientType.TO,
            //InternetAddress.parse("sasi.reddy@diamondresorts.com, Phil.Fortner@diamondresorts.com, Leo.Luz@diamondresorts.com, Michael.Pascucci@diamondresorts.com, Swathi.Yadagani@diamondresorts.com, sri.godugula@diamondresorts.com"));
        
        message.setRecipients(Message.RecipientType.TO,
              InternetAddress.parse("sasi.reddy@diamondresorts.com"));
          
        
        //Subject
        message.setSubject(SuiteName+" Report");
        
        //Body Message
        BodyPart messageBodyPart1 = new MimeBodyPart();
        messageBodyPart1.setText(" Hi All,  "
                       + "\n\n Please review the following automation report executed in TEST database. There may be failed scenarios that require your attention." +
           "\n\n Thanks,"); //Enter the Message.
       
      
        String file = Testoutput_Filepath; // Path of file
        String filename = reportFileName;
        MimeBodyPart messageBodyPart2 = new MimeBodyPart();  
           DataSource source = new FileDataSource(file+filename);  
             messageBodyPart2.setDataHandler(new DataHandler(source));  
               messageBodyPart2.setFileName("Clarity Automation Report.html");  
        

        Multipart multipart = new MimeMultipart();  
        
        //add
        multipart.addBodyPart(messageBodyPart1);
    
        //add
        multipart.addBodyPart(messageBodyPart2);  
    
        message.setContent(multipart );  

        System.out.println("Sending");
        
        //Send
        Transport.send(message);
    
        System.out.println("Done");
    
      } catch (MessagingException e) {
          throw new RuntimeException(e);
          
          
      }
    }
}