package dbconnectivity;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import login.Common;


public class UpdateLeads extends Common  {
	
	Logger logger = Logger.getLogger("SM_Update_Leads.class");
	
	 	@Test
	 	@Parameters("Database")
		public void Update(String Database) throws SQLException {
	 		PropertyConfigurator.configure("Log4j.properties");
		
		 Connection connection = null;
		   PreparedStatement prepared = null;
		   
		try {

			Class.forName("oracle.jdbc.driver.OracleDriver");

		} catch (ClassNotFoundException e) {

			logger.info("Oracle JDBC server not found");
			e.printStackTrace();
			return;
		}
		
		logger.info("Oracle JDBC Driver Registered!");
		try {
			connection = DriverManager.getConnection(Database,UsernameDB,PasswordDB);
			logger.info("Connected to Database");

		} catch (SQLException e) {

			logger.info("Connection Failed! Check output console");
			e.printStackTrace();
			return;

		}

		if (connection != null) {
			logger.info("Update LED_ADDRESS");
			
			prepared = connection.prepareStatement("update Premier.LED_ADDRESS SET ADDR1 = ?, CITY = ?, STATE_CODE = ?, FMT_POSTAL_CODE = ? "
					+ "where LEAD_ID in (1606850, 175804616, 4617274, 6465951, 316950932, 5137804, 609098016, 660462, 883711, 282096260, 211777, 8776663, 4270760, 82140470)");
					
			prepared.setString(1, Address);
			prepared.setString(2, "Las Vegas");
			prepared.setString(3, "NV");
			prepared.setString(4, PostalCode);
			prepared.executeUpdate();
			
			logger.info("Successfully Updated Address, city, state, postal code: DONE");
			
		} else {
			logger.info("Failed to make connection!");
		}
		
		if (connection != null) {
			logger.info("Update P_LEAD");
			
			prepared = connection.prepareStatement("update Premier.P_LEAD SET "
					+ "Premier.P_LEAD.FMT_HOME_PHONE = ?, "
					+ "Premier.P_LEAD.EMAIL_ADDRESS= ? "
					+ "where LEAD_ID in (1606850, 175804616, 4617274, 6465951, 316950932, 5137804, 609098016, 660462, 883711, 282096260, 211777, 8776663, 4270760, 82140470)");
					
			prepared.setString(1, "2022022222");
			prepared.setString(2, "Tester@example.com");
			prepared.executeUpdate();
			logger.info("Updated Phone Number and Email Address Successfully: DONE");
			logger.info("Finished");
			
		} else {
			logger.info("Failed to make connection!");
		}
		
	}

}
